<div class="layered layered-category">
    <div class="layered-content">
        <ul class="tree-menu">
            @php
                $main = DB::table('categories')->get();
            @endphp
            @foreach ($main as $cat)
                {{-- expr --}}
            <li>
                <span></span><a href="{{ url('maincategorypage/'.$cat->category_id) }}">{{ $cat->category_name }}</a>
                <ul>
                    <li><span></span><a href="#">T-shirts</a></li>
                    <li><span></span><a href="#">Dresses</a></li>
                    <li><span></span><a href="#">Casual</a></li>
                    <li><span></span><a href="#">Evening</a></li>
                    <li><span></span><a href="#">Summer</a></li>
                    <li><span></span><a href="#">Bags & Shoes</a></li>
                    <li><span></span><a href="#"><span></span>Blouses</a></li>
                </ul>
            </li>
          
            @endforeach
        </ul>
    </div>
</div>