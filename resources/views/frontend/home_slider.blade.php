<div id="home-slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 slider-left"></div>
            <div class="col-sm-9 header-top-right">
                <div class="homeslider">
                    <div class="content-slide">
                        <ul id="contenhomeslider">
                         @foreach ($sliders as $element)
                           <li><img style="height:400px;" alt="Funky roots" src="{{ asset('/'.$element->slider_image) }}" title="Funky roots" /></li>
                         @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>