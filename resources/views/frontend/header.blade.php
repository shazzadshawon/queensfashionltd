
@php
    $main_menus = \DB::table('categories')->get();
    $mainmenus = \DB::table('categories')->where('mega_menu','!=',1)->take(4)->get();
@endphp


<div id="header" class="header">
    <div class="top-header">
        <div class="container">
            <div class="nav-top-links">
                <a class="first-item" href="#"><img alt="phone" src="{{ asset('assets/images/phone.png') }}" />+88 019 96844037</a>
            </div>
    {{--         
            <div class="support-link">
                <a href="#">Services</a>
                <a href="#">Support</a>
            </div>
 --}}
            <div id="user-info-top" class="user-info pull-right">
                <div class="dropdown">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>My Account</span></a>
                    <ul class="dropdown-menu mega _dropdown" role="menu">
                         @if (Session::has('customer_id'))
                             <li><a href="{{ url('logout') }}">logout ({{ Session::get('customer_name') }})</a></li>
                             <li><a href="{{ url('view-wishlist') }}">Wishlists</a></li>
                         @else
                             <li><a href="{{ asset('User-Register') }}">Login / Register</a></li>
                         @endif
                       
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--/.top-header -->
    <!-- MAIN HEADER -->
    
    <div class="container main-header">
        <div class="row">
            <div class="col-xs-12 col-sm-3 logo">
                <a href="{{ url('/') }}"><img alt="Lakes Point" src="{{ asset('assets/images/queen_logo.png') }}" /></a>
            </div>            
            @include('frontend/header_search')
            @include('frontend/shopping_cart')
        </div>    
    </div>

    <!-- END MAIN HEADER -->
    
@if(\Request::url() === 'http://queenfashionworld.com')
    @include('frontend.navbar-home')
@else
    @include('frontend.navbar')
@endif









</div>