
 @php
                         $i=0;
                         $j=0;
                        $total=0;
                        $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get(); 
                                              
                        foreach($addTocart as $cart_info)
                        {
                              $i = $cart_info->product_quantity+$i;
                              $total = ($cart_info->product_price*$cart_info->product_quantity)+$total;
                        }
                      
@endphp

<div id="cart-block" class="col-xs-5 col-sm-2 shopping-cart-box">
    <a class="cart-link" href="{{  url('cart')  }}">
        <span class="title">Shopping Cart</span>
        <span class="total">{{ $i }} item - &#2547; {{ $total }}</span>
        <span class="notify notify-left">{{ $i }}</span>
    </a>
    <div class="cart-block">
        <div class="cart-block-content">
            <h5 class="cart-title">{{ $i }} Items in my cart</h5>
            <div class="cart-block-list">
                <ul>
                    
                   @foreach ($addTocart as $cart)
                   @php
                       $product = DB::table('products')->where('id',$cart->product_id)->first();
                       $image = DB::table('product_images')->where('product_id',$product->id)->first();

                   @endphp
                    <li class="product-info">
                        <div class="p-left">
                            {{-- <a href="#" class="remove_link"></a> --}}
                            <a href="{{ url('product-details/'.$product->id) }}">
                            <img class="img-responsive" src="{{ asset('product_image/'.$image->product_image) }}" alt="p10">
                            </a>
                        </div>
                        <div class="p-right">
                            <p class="p-name">{{ $product->product_name }}</p>
                            <p class="p-rice">&#2547; {{ $cart->product_price * $cart->product_quantity}}</p>
                            @php
                                $j = $cart->product_price * $cart->product_quantity +$j;
                            @endphp
                            <p>Qty: {{ $cart->product_quantity }}</p>
                        </div>
                    </li>
                   @endforeach

                  
                </ul>
            </div>
            <div class="toal-cart">
                <span>Total</span>
                <span class="toal-price pull-right">&#2547; {{ $j }}</span>
            </div>
            <div class="cart-buttons">

            	 <a href="{{ URL::to('/cart') }}" class="btn-check-out">
                                            
                                                 Cart
                                           
                                </a>
                 @if (Session::has('customer_id'))
                                <a href="{{ URL::to('/shipping') }}" class="btn-check-out">
                                            
                                                 Checkout
                                           
                                </a>
                                 @else
                                <a href="{{ URL::to('/User-Register') }}" class="btn-check-out">
                                    
                                                 Checkout
                                         
                                </a>
                                 @endif


                
            </div>
        </div>
    </div>
</div>