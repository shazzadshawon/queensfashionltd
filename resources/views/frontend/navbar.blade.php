
<div id="nav-top-menu" class="nav-top-menu">
    <div class="container">
        <div class="row">

        	@php
        		$categories = DB::table('categories')->where('publication_status',1)->get();
        		$j=10;
        	@endphp
            

            <div class="col-sm-3" id="box-vertical-megamenus">
                <div class="box-vertical-megamenus">
                    <h4 class="title">
                        <span class="title-menu">Categories</span>
                        <span class="btn-open-mobile pull-right home-page"><i class="fa fa-bars"></i></span>
                    </h4>

                </div>
            </div>
            


            <div id="main-menu" class="col-sm-9 main-menu">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <i class="fa fa-bars"></i>
                            </button>
                            <a class="navbar-brand" href="#">MENU</a>
                        </div>
                        @php
			        		$categories = DB::table('categories')->where('publication_status',1)->get();
			        		
			        	@endphp
                        <div id="navbar" class="navbar-collapse collapse">
                            
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="{{ url('/') }}">Home</a></li>
@for ($i = 0; $i < 4; $i++)

                                <li class="dropdown">
                                    <a href="{{ url('maincategorypage/'.$categories[$i]->category_id) }}" class="dropdown-toggle" data-toggle="dropdown">{{$categories[$i]->category_name}}</a>
                                       @php
									  $sub_categories = DB::table('sub_categories')->where('category_id',$categories[$i]->category_id)->where('publication_status',1)->get();
									   @endphp
									   @if (!empty($sub_categories))
									   	 <ul class="mega_dropdown dropdown-menu" style="width: 830px;">
									   	 	@foreach ($sub_categories as $sub)
										   	 <li class="block-container col-sm-4">
	                                            <ul class="block">
	                                                <li class="link_container group_header">
	                                                    <a href="{{ url('categorypage/'.$sub->sub_category_id) }}">{{ $sub->sub_category_name }}</a>
	                                                </li>
	                                                   @php
  											$sub_sub_categories = DB::table('sub_sub_categories')->where('sub_category_id',$sub->sub_category_id)->where('publication_status',1)->get();
                    					@endphp
                    					@if (!empty ($sub_sub_categories))
                    						@foreach ($sub_sub_categories as $sub_sub)
                    							 <li class="link_container">
	                                                    <a href="{{ url('subcategorypage/'.$sub_sub->id) }}">{{ $sub_sub->sub_sub_category_name }}</a>
	                                                </li>
                    						@endforeach
	                                               
	                                               
										@endif
	                                            </ul>
	                                        </li>
									   	 	@endforeach
                                     


                                    </ul>
									   @endif
                                   
                                </li>
@endfor
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </nav>
            </div>
        </div>
        <!-- userinfo on top-->
        <div id="form-search-opntop">
        </div>
        <!-- userinfo on top-->
        <div id="user-info-opntop">
        </div>
        <!-- CART ICON ON MMENU -->
        <div id="shopping-cart-box-ontop">
            <i class="fa fa-shopping-cart"></i>
            <div class="shopping-cart-box-ontop-content"></div>
        </div>
    </div>
</div>

