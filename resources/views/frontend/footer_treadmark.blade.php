<div id="trademark-box" class="row">
    <div class="col-sm-12">
        <ul id="trademark-list" class="owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "0" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}' autoplay>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/aarong.png') }}"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/bata.png') }}"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/rong.jpg') }}"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/walton.png') }}"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/xiomi.png') }}"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/trademark-ups.jpg') }}"  alt="ups"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/trademark-qiwi.jpg') }}"  alt="ups"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/trademark-wu.jpg') }}"  alt="ups"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/trademark-visa.jpg') }}"  alt="ups"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/trademark-mc.jpg') }}"  alt="ups"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/trademark-ems.jpg') }}"  alt="ups"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/trademark-dhl.jpg') }}"  alt="ups"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/trademark-fe.jpg') }}"  alt="ups"/></a>
                </div>
            </li>
            <li>
                <div class="left-block">
                    <a href="#"><img src="{{ asset('assets/data/treadmark/trademark-wm.jpg') }}"  alt="ups"/></a>
                </div>
            </li>
        </ul>
    </div>
</div> <!-- /#trademark-box -->