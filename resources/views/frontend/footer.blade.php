

<footer id="footer">
     <div class="container">
            <!-- introduce-box -->
            <div id="introduce-box" class="row">
                <div class="col-md-3">
                    <div id="address-box">
                        <a href="{{ url('/') }}"><img src="{{ asset('assets/images/queen_logo.png') }}" alt="logo" /></a>
                        <div id="address-list">
                            <div class="tit-name">Address:</div>
                            <div class="tit-contain">House# 1206/1, East Monipur, Mirpur-2, Dhaka</div>
                            <div class="tit-name">Phone:</div>
                            <div class="tit-contain">+88 019 96844037</div>
                            <div class="tit-name">Email:</div> <br>
                            <div class="tit-contain"> <a href="mailto:shahinoor.alam@queensfashionbd.com">shahinoor.alam@queensfashionbd.com</a></div>
                            <div class="tit-contain"> <a href="mailto:asgar.ali@queensfashionbd.com"> asgar.ali@queensfashionbd.com</a></div>
                            <div class="tit-contain"> <a href="mailto:help@queensfashionbd.com">help@queensfashionbd.com</a></div>
                        </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="introduce-title">Company</div>
                            <ul id="introduce-company"  class="introduce-list">
                                <li><a href="{{ url('about-us') }}">About Us</a></li>
                                <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                                <li><a href="{{ url('return-policy') }}">Return Policy</a></li>
                                <li><a href="{{ url('refund') }}">Refund</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <div class="introduce-title">My Account</div>
                            <ul id = "introduce-Account" class="introduce-list">
                               
                                <li><a href="{{ url('shipping') }}">My Order</a></li>
                                <li><a href="{{ url('view-wishlist') }}">My Wishlist</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div id="contact-box">
                       {{--  <div class="introduce-title">Newsletter</div>
                        <div class="input-group" id="mail-box">
                          <input type="text" placeholder="Your Email Address"/>
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button">OK</button>
                          </span>
                        </div><!-- /input-group --> --}}
                        <div class="introduce-title">Let's Socialize</div>
                        <div class="social-link">
                            <a href="https://www.facebook.com/ladiesfashion2017"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.instagram.com/queens_fashion_world786/"><i class="fa fa-instagram" aria-hidden="true"></i>
</a>
                            
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                    
                </div>
            </div><!-- /#introduce-box -->
        
            <!-- #trademark-box -->
            
            

             <div id="footer-menu-box">
                <p class="text-center">Created with <i class="fa fa-heart" style="color:red" aria-hidden="true"></i> <a href="http://shobarjonnoweb.com/" target="_blank">Shobarjonnoweb.com</a></p>
            </div><!-- /#footer-menu-box -->
     </div>
</footer>










