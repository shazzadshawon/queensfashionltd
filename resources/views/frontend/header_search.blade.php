
<div class="col-xs-7 col-sm-7 header-search-box">
@php
  $categories = DB::table('categories')->get();
@endphp
    <form class="form-inline" action="product_search" method="get">
    
 
        <div class="form-group form-category">
            <select class="select-category" name="cat" style="width: 20px;">
            <option value="0">All Categories</option>
            @foreach ($categories as $cat)
              <option value="{{ $cat->category_id }}">{{ $cat->category_name }}</option>
            @endforeach
              
            </select>
          </div>
      
         <div class="form-group input-serach">
            <input type="text" name="query" required placeholder="Keyword here...">
          </div>
     
        <button type="submit" class="pull-right btn-search"></button>
     
          
         
          
    </form>
</div>