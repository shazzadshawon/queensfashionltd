<div class="tab-panel active" id="tab-pants">   <!-- PANTS -->
    <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "0" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
        
        @include('frontend/catagory_feature_tab_detail_list_item')

        <li>
            <div class="left-block">
                <a href="detail.php">
                <img class="img-responsive" alt="pants" src="assets/data/mens_fashion/pants/pant3.jpg" /></a>
                <div class="quick-view">
                        <a title="Add to my wishlist" class="heart" href="#"></a>
                        <a title="Add to compare" class="compare" href="#"></a>
                        <a title="Quick view" class="search" href="#"></a>
                </div>
                <div class="add-to-cart">
                    <a title="Add to Cart" href="#">Add to Cart</a>
                </div>
            </div>
            <div class="right-block">
                <h5 class="product-name"><a href="detail.php">Pant 08</a></h5>
                <div class="content_price">
                    <span class="price product-price">&#2547;3,950</span>
                    <span class="price old-price">&#2547;4,950</span>
                </div>
                <div class="product-star">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                    <i class="fa fa-star-o"></i>
                    <i class="fa fa-star-o"></i>
                </div>
            </div>
        </li>
        <li>
            <div class="left-block">
                <a href="detail.php">
                <img class="img-responsive" alt="pants" src="assets/data/mens_fashion/pants/pant5.jpg" /></a>
                <div class="quick-view">
                        <a title="Add to my wishlist" class="heart" href="#"></a>
                        <a title="Add to compare" class="compare" href="#"></a>
                        <a title="Quick view" class="search" href="#"></a>
                </div>
                <div class="add-to-cart">
                    <a title="Add to Cart" href="#">Add to Cart</a>
                </div>
            </div>
            <div class="right-block">
                <h5 class="product-name"><a href="detail.php">Pant 05</a></h5>
                <div class="content_price">
                    <span class="price product-price">&#2547;3,950</span>
                    <span class="price old-price">&#2547;4,950</span>
                </div>
                <div class="product-star">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                    <i class="fa fa-star-o"></i>
                    <i class="fa fa-star-o"></i>
                </div>
            </div>
        </li>
        <li>
            <div class="left-block">
                <a href="detail.php">
                <img class="img-responsive" alt="pants" src="assets/data/mens_fashion/pants/pant6.jpg" /></a>
                <div class="quick-view">
                        <a title="Add to my wishlist" class="heart" href="#"></a>
                        <a title="Add to compare" class="compare" href="#"></a>
                        <a title="Quick view" class="search" href="#"></a>
                </div>
                <div class="add-to-cart">
                    <a title="Add to Cart" href="#">Add to Cart</a>
                </div>
            </div>
            <div class="right-block">
                <h5 class="product-name"><a href="detail.php">Pant 06</a></h5>
                <div class="content_price">
                    <span class="price product-price">&#2547;3,950</span>
                    <span class="price old-price">&#2547;4,950</span>
                </div>
                <div class="product-star">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                    <i class="fa fa-star-o"></i>
                    <i class="fa fa-star-o"></i>
                </div>
            </div>
        </li>
        <li>
            <div class="left-block">
                <a href="detail.php">
                <img class="img-responsive" alt="pants" src="assets/data/mens_fashion/pants/pant8.jpg" /></a>
                <div class="quick-view">
                        <a title="Add to my wishlist" class="heart" href="#"></a>
                        <a title="Add to compare" class="compare" href="#"></a>
                        <a title="Quick view" class="search" href="#"></a>
                </div>
                <div class="add-to-cart">
                    <a title="Add to Cart" href="#">Add to Cart</a>
                </div>
            </div>
            <div class="right-block">
                <h5 class="product-name"><a href="detail.php">Pant 08</a></h5>
                <div class="content_price">
                    <span class="price product-price">&#2547;3,950</span>
                    <span class="price old-price">&#2547;4,950</span>
                </div>
                <div class="product-star">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                    <i class="fa fa-star-o"></i>
                    <i class="fa fa-star-o"></i>
                </div>
            </div>
        </li>
        <li>
            <div class="left-block">
                <a href="detail.php">
                <img class="img-responsive" alt="pants" src="assets/data/mens_fashion/pants/pant7.jpg" /></a>
                <div class="quick-view">
                        <a title="Add to my wishlist" class="heart" href="#"></a>
                        <a title="Add to compare" class="compare" href="#"></a>
                        <a title="Quick view" class="search" href="#"></a>
                </div>
                <div class="add-to-cart">
                    <a title="Add to Cart" href="#">Add to Cart</a>
                </div>
            </div>
            <div class="right-block">
                <h5 class="product-name"><a href="detail.php">Pant 07</a></h5>
                <div class="content_price">
                    <span class="price product-price">&#2547;3,950</span>
                    <span class="price old-price">&#2547;4,950</span>
                </div>
                <div class="product-star">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                    <i class="fa fa-star-o"></i>
                    <i class="fa fa-star-o"></i>
                </div>
            </div>
        </li>
    </ul>
</div>