@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->
{{-- @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h4 style="text-align: center;"> {{Session::get('message')}}</h4>
</div>
      
@endif --}}
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{ url('/') }}" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Cart</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">
                                            
                                                Shopping Cart
                                          
            </span>
        </h2>
   
        <div class="page-content checkout-page">
          
           
    
            <div class="box-border">
                <table class="table table-bordered table-responsive cart_summary">
                    <thead>
                        <tr>
                            <th class="cart_product">
                                            
                                                 Product
                                            
                            
                            </th>
                            <th>
                                            
                                                 Description
                                            
                            
                            </th>
                            <th>
                                           
                                                 Size
                                           
                            
                            </th>
                            <th>
                                           
                                                 Unit price
                                           
                                
                            </th>
                            <th>
                                            
                                                 Qty
                                           
                            
                            </th>
                            <th>
                                            
                                                Total
                                           
                            </th>
                            <th  class="action"><i class="fa fa-trash-o"></i></th>
                        </tr>
                    </thead>
                    <tbody>

                    @php

                        $i=1;
                        $j=0;
                        $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get(); 
                        
                            @endphp
                        @foreach ($addTocart as $cart)
                           
                           @php
                               $addTocartImage = DB::table('product_images')->where('product_id', $cart->product_id)->first(); 
                           @endphp
                        <tr>
                            <td class="cart_product">
                                <a href="{{ URL::to('/product-details/'.$cart->product_id) }}"><img src="{{asset('product_image/'.$addTocartImage->product_image)}}" alt="Product"></a>
                            </td>
                            <td class="cart_description">
                                <p class="product-name"><a href="{{ URL::to('/product-details/'.$cart->product_id) }}">
                                           
                                               {{ $cart->product_name }} 
                                           
                                
                                </a></p>
                                <small class="cart_ref">Item Code : #{{ $cart->product_code }}</small><br>
                                {{-- <small><a href="#">Color : Beige</a></small><br>   
                                <small><a href="#">Size : S</a></small> --}}
                            </td>
                            <td class="price"><span> {{ $cart->size }} </span></td>
                            <td class="price"><span>
                                          @php
                       
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $BnPrice = str_replace($replace_array,$search_array,$cart->product_price);
                                            @endphp
                                     
                                           
                                                 {{ $cart->product_price }} TK
                                           
                            
                            </span></td>
                            <td class="qty">
                                <h3>
                                            @php
                       
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $QTY = str_replace($replace_array,$search_array,$cart->product_quantity);
                                            @endphp
                                     
                                          
                                                 {{ $cart->product_quantity }}
                                           
                             
                                </h3>
                            </td>
                            <td class="price">
                                <span>
                                       @php
    $sub_total=$cart->product_price*$cart->product_quantity;
                       
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $SubPrice = str_replace($replace_array,$search_array,$sub_total);
                                            @endphp
                                     
                                          
                                                 {{ $sub_total }} TK
                                           
                               

                                </span>
                            </td>
                            <td class="action">
                                <a href="{{URL::to('/remove-cart-product/'.$cart->id)}}">Delete item</a>
                            </td>
                        </tr>

                       @php
                                     $j =$j+ $sub_total;
                                     $i++; 
                                @endphp
                                @endforeach

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2" rowspan="2"></td>
                            <td colspan="3">
                            <b>
                                           
                                              Grand Total
                                           
                             </b>
                             </td>
                            <td colspan="2">
                                    @php
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $BnT = str_replace($replace_array,$search_array,$j);
                                    @endphp
                                           
                                                 {{ $j }} TK
                                           
                            </td>
                        </tr>
                        {{-- <tr>
                            <td colspan="3"><strong>Total</strong></td>
                            <td colspan="2"><strong>122.38 €</strong></td>
                        </tr> --}}
                    </tfoot>    
                </table>
                                                <a href="{{ url('shipping') }}" class="button pull-right">Chechout</a>
                
            </div>
        </div>
    </div>
</div>
</div>
@endsection