<!DOCTYPE html>
<html class=""><!--<![endif]-->
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>ArtandCreativeinterior</title>

    <!-- Standard Favicon -->
    <link rel="icon" type="image/x-icon" href="../icon/LOGO.jpg" />
    
    <!-- For iPhone 4 Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images//apple-touch-icon-114x114-precomposed.png">
    
    <!-- For iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images//apple-touch-icon-72x72-precomposed.png">
    
    <!-- For iPhone: -->
    <link rel="apple-touch-icon-precomposed" href="images//apple-touch-icon-57x57-precomposed.png"> 
    
    <!-- Custom - Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('style.css') }}">
    
    <!--[if lt IE 9]>
        <script src="js/html5/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .photo-slider .item {
    padding: 10px 0;
}
.carousel-caption {
  
    padding-bottom: 0px;
}
.middle-header {
    margin-top: 5px;
    margin-bottom: 10px;
}

    </style>
    
</head>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">
    <!-- Loader -->
    {{-- <div id="site-loader" class="load-complete">
        <div class="loader">
            <div class="loader-inner ball-clip-rotate">
                <div></div>
            </div>
        </div>
    </div> --}}<!-- Loader /- -->
    
    
    <!-- Header -->
    @include('pages.header')
    <!-- Header /- -->
    
    <main class="site-main page-spacing">
        <!-- Page Banner -->
        <div class="page-banner shop-banner container-fluid no-padding">
            <div class="page-banner-content">
                <h3>Offer Products</h3>
            </div>
        </div><!-- Page Banner /- -->
        
        
        <!-- Shop 2 -->
        <div class="latest-product shop-2 container-fluid no-padding woocommerce">
            <div class="section-padding"></div>
            <!-- Container -->
            <div class="container">
                <ul class="products">

                    @foreach ($products as $productsInfo)
                       
                   
                     <li class="product">                            
                        <a href="{{ URL::to('/product-details/'.$productsInfo->id) }}" title="Prouct">
                            @if($productsInfo->discount>0)
                            <span class="onsale">{{ $productsInfo->discount }}%</span>
                            @endif
                            @php
                             $productsImage = DB::table('product_images')->where('product_id',$productsInfo->id)->first();
                            @endphp
                            <span class="product-img">
                                <img src="../{{ $productsImage->product_image}}" width="270" height="360" alt="product" style="height: 360px; width: 270px;" />
                            </span>
                            <h3>{{ $productsInfo->product_name }}</h3>
                           @if($productsInfo->discount>0)
                                <span class="price"><span class="amount">{{ $productsInfo->product_price-($productsInfo->product_price*$productsInfo->discount)/100 }} TK</span><del>{{ $productsInfo->product_price }} TK</del></span>
                            @else
                            <span class="price"><span class="amount">{{ $productsInfo->product_price }} TK</span></span>
                            @endif
                        </a>
                        {{-- <p class="hover-content">
                            <a title="Add To Cart" href="#" class="button product_type_simple add_to_cart_button"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Add to cart</a>
                            <span>
                                <a href="#" class="icons"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                <a href="#" class="icons"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                <a href="#" class="icons"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                            </span>
                        </p> --}}
                    </li>

                     @endforeach
    
                </ul>
                {{-- <nav class="ow-pagination">
                    <ul class="pagination">
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
                    </ul>
                </nav> --}}
            </div><!-- Container /- -->
            <div class="section-padding"></div>
        </div><!-- Shop 2 /- -->
    </main>
    
    <!-- Footer Main -->
    @include('pages.footer')
    <!-- Footer Main /- -->
    
    <!-- Bottom Footer -->
    <!-- Bottom Footer /- -->
    <!-- JQuery v1.11.3 -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Library - Js -->
    <script src="{{ asset('libraries/lib.js') }}"></script>
    <!-- Bootstrap JS File v3.3.5 -->
    
    <script src="{{ asset('libraries/jquery.countdown.min.js') }}"></script>
    
    <script src="{{ asset('libraries/lightslider-master/lightslider.js') }}"></script>
    
    <script src="{{ asset('libraries/slick/slick.min.js') }}"></script>

    <!-- Library - Google Map API -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    
    <!-- Library - Theme JS -->
    <script src="{{ asset('js/functions.js') }}"></script>
</body>
</html>