@extends('layouts.frontend')

@section('content')


<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="index.php" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Contact</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Contact Us</span>
        </h2>
        <!-- ../page heading-->
        <div id="contact" class="page-content page-contact">
            <div id="message-box-conact"></div>
            <div class="row">
               
 <form action="{{ url('post_contact') }}" method="POST">
                {{ csrf_field() }}
                    <div class="col-sm-6">
                        <h3 class="page-subheading">CONTACT FORM</h3>
                        <div class="contact-form-box">
                            <div class="form-selector">
                                <label>Subject Heading</label>
                                <select class="form-control input-sm" id="subject" name="contact_title">
                                    <option value="Customer service">Customer service</option>
                                    <option value="Webmaster">Webmaster</option>
                                </select>
                            </div>
                            <div class="form-selector">
                                <label>Email address</label>
                                <input type="text" class="form-control input-sm" id="email"  name="contact_email" />
                            </div>
                            <div class="form-selector">
                                <label>Order reference</label>
                                <input type="text" class="form-control input-sm" id="order_reference"  name="contact_reference"/>
                            </div>
                            <div class="form-selector">
                                <label>Message</label>
                                <textarea class="form-control input-sm" rows="10" id="message"  name="contact_description"></textarea>
                            </div>
                            <div class="form-selector">
                                <button id="btn-send-contact" type="submit" class="btn">Send</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-xs-12 col-sm-6" id="contact_form_map">
                    <h3 class="page-subheading">Information</h3>
                    <p>Queen's Fashion World  a massive selection of great products from hundreds of leading national and international brands.  Queen's Fashion World offers women's men's and kids' apparel, footwear, handbags, fashion accessories, beauty products and much more, all under the same roof.</p>
                    <br/>
                    <ul class="store_info">
                        <li><i class="fa fa-home"></i>Our business address is House# 1206/1, East Monipur, Mirpur-2</li>
                        <li><i class="fa fa-phone"></i><span>+88 019 96844037</span></li>
                        <li><i class="fa fa-envelope"></i>Email: <span><a href="mailto:shahinoor.alam@queenfashionworld.com">shahinoor.alam@queenfashionworld.com</a></span></li>
                        <li><i class="fa fa-envelope"></i>Email: <span><a href="mailto:asgar.ali@queenfashionworld.com">asgar.ali@queenfashionworld.com</a></span></li>
                        <li><i class="fa fa-envelope"></i>Email: <span><a href="mailto:help@queenfashionworld.com">help@queenfashionworld.com</a></span></li>
                    </ul>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="map-outer">
                                <!--Map Canvas-->
                                <div class="map-canvas"
                                     data-zoom="16"
                                     data-lat="23.7783402"
                                     data-lng="90.3618536"
                                     data-type="roadmap"
                                     data-hue="#ffc400"
                                     data-title="Queen's Fashion"
                                     data-content="House# 1206/1, East Monipur, Mirpur-2<br><a href='mailto:help@queenfashionworld.com'>help@queenfashionworld.com</a>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="http://maps.google.com/maps/api/js?key=AIzaSyDElOclVeevpAwrC01r05nJSI6Bri8dSXA"></script>

<script type="text/javascript" src="{{ asset('assets/js/map-script.js') }}"></script>



@endsection












