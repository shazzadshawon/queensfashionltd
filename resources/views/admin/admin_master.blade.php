<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- start: Meta -->
        <meta charset="utf-8">
        <title>Admin Panel</title>
        <meta name="description" content="Queens Fashions">
        <meta name="author" content="Queens Fashions">
        <meta name="keyword" content="Queens Fashions">
        <!-- end: Meta -->

        <!-- start: Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- end: Mobile Specific -->

        <!-- start: CSS -->
        <link id="bootstrap-style" href="{{asset('admin_asset/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('admin_asset/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
        <link id="base-style" href="{{asset('admin_asset/css/style.css')}}" rel="stylesheet">
        <link id="base-style-responsive" href="{{asset('admin_asset/css/style-responsive.css')}}" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
        <!-- end: CSS -->

                <script src="{{asset('admin_asset/js/jquery-1.9.1.min.js')}}"></script>
        <script src="{{asset('admin_asset/js/jquery-migrate-1.0.0.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery-ui-1.10.0.custom.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.ui.touch-punch.js')}}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>

        <script src="{{asset('admin_asset/js/modernizr.js')}}"></script>

        <script src="{{asset('admin_asset/js/bootstrap.min.js')}}"></script>

        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <link id="ie-style" href="css/ie.css" rel="stylesheet">
        <![endif]-->

        <!--[if IE 9]>
                <link id="ie9style" href="css/ie9.css" rel="stylesheet">
        <![endif]-->

        <!-- start: Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico">
        <!-- end: Favicon -->

        <script type="text/javascript">
            function checkDelete(){
            chk = confirm('Are You Sure to Delete This ?');
            if (chk){
            return true;
            } else{
            return false;
            }
            }
        </script>

<script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    </head>

    <body>
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1923089044631336";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <!-- start: Header -->
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="{{URL::to('/dashboard')}}"><span>Admin Panel</span></a>

                    <!-- start: Header Menu -->
                    <div class="nav-no-collapse header-nav">
                        <ul class="nav pull-right">
                           
                            <!-- start: User Dropdown -->
                            <li class="dropdown">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="halflings-icon white user"></i>  {{Auth::user()->name}}
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-menu-title">
                                        <span>Account Settings</span>
                                    </li>
                                   {{--  <li><a href="#"><i class="halflings-icon user"></i> Profile</a></li> --}}
                                    <li>
                                
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                 </li>
                                </ul>
                            </li>
                            <!-- end: User Dropdown -->
                        </ul>
                    </div>
                    <!-- end: Header Menu -->

                </div>
            </div>
        </div>
        <!-- start: Header -->

        <div class="container-fluid-full">
            <div class="row-fluid">

                <!-- start: Main Menu -->
                <div id="sidebar-left" class="span2" style="overflow-y: scroll;">
                    <div class="nav-collapse sidebar-nav">
                        <ul class="nav nav-tabs nav-stacked main-menu">
                            <li>
                                <div class="" align="center" style="margin: 30px">
                                    <img src="{{ asset('public/sjw.png') }}">
                                </div>
                            </li>
                            <li></li>
                            <li><a href="{{URL::to('/dashboard')}}"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>
                             {{-- <li><a href="{{URL::to('/add-admin')}}"><i class="icon-save"></i><span class="hidden-tablet"> Add Admin</span></a></li> --}}	
                            <li><a href="{{URL::to('/add-category')}}"><i class="icon-save"></i><span class="hidden-tablet"> Add Main Category</span></a></li>
                            <li><a href="{{URL::to('/manage-category')}}"><i class="icon-folder-open"></i><span class="hidden-tablet">Manage Main Category</span></a></li>
                            <li><a href="{{URL::to('/add-sub-category')}}"><i class="icon-save"></i><span class="hidden-tablet"> Add  Category</span></a></li>
                            <li><a href="{{URL::to('/manage-sub-category')}}"><i class="icon-folder-open"></i><span class="hidden-tablet">Manage  Category</span></a></li>
                            <li><a href="{{URL::to('/add-sub-sub-category')}}"><i class="icon-save"></i><span class="hidden-tablet"> Add  Sub Category</span></a></li>
                            <li><a href="{{URL::to('/manage-sub-sub-category')}}"><i class="icon-folder-open"></i><span class="hidden-tablet">Manage  Sub Category</span></a></li>

<li><a href="{{URL::to('/add-offer')}}"><i class="icon-save"></i><span class="hidden-tablet"> Add Offer</span></a></li>
<li><a href="{{URL::to('/manage-offer')}}"><i class="icon-folder-open"></i><span class="hidden-tablet">Manage Offer</span></a></li>

                            <li><a href="{{URL::to('/add-slider-image')}}"><i class="icon-save"></i><span class="hidden-tablet"> Add Slider Image</span></a></li>
                            <li><a href="{{URL::to('/manage-slider-image')}}"><i class="icon-folder-open"></i><span class="hidden-tablet">Manage Slider Image</span></a></li>
                             <li><a href="{{URL::to('/add-product')}}"><i class="icon-save"></i><span class="hidden-tablet"> Add Product</span></a></li>
                             <li><a href="{{URL::to('/manage-product')}}"><i class="icon-folder-open"></i><span class="hidden-tablet">Manage Product</span></a></li>
                             <li><a href="{{URL::to('/manage-order')}}"><i class="icon-folder-open"></i><span class="hidden-tablet">Manage Order</span></a></li>
                             <li><a href="{{URL::to('/complain')}}"><i class="icon-folder-open"></i><span class="hidden-tablet">Complain Box</span></a></li>
                             
                            {{--  <li><a href="{{URL::to('/show-subscribe')}}"><i class="icon-folder-open"></i><span class="hidden-tablet">View Subscribe User</span></a></li> --}}

                            <li><a href="{{URL::to('/show-message')}}"><i class="icon-folder-open"></i><span class="hidden-tablet">Messages</span></a></li>
                             
<!--                            <li><a href="widgets.html"><i class="icon-dashboard"></i><span class="hidden-tablet"> Widgets</span></a></li>-->
<!--                            <li>
                                <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Dropdown</span><span class="label label-important"> 3 </span></a>
                                <ul>
                                    <li><a class="submenu" href="submenu.html"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 1</span></a></li>
                                    <li><a class="submenu" href="submenu2.html"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 2</span></a></li>
                                    <li><a class="submenu" href="submenu3.html"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 3</span></a></li>
                                </ul>	
                            </li>
                            <li><a href="form.html"><i class="icon-edit"></i><span class="hidden-tablet"> Forms</span></a></li>
                            <li><a href="chart.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> Charts</span></a></li>
                            <li><a href="typography.html"><i class="icon-font"></i><span class="hidden-tablet"> Typography</span></a></li>
                            <li><a href="gallery.html"><i class="icon-picture"></i><span class="hidden-tablet"> Gallery</span></a></li>
                            <li><a href="table.html"><i class="icon-align-justify"></i><span class="hidden-tablet"> Tables</span></a></li>
                            <li><a href="calendar.html"><i class="icon-calendar"></i><span class="hidden-tablet"> Calendar</span></a></li>
                            <li><a href="file-manager.html"><i class="icon-folder-open"></i><span class="hidden-tablet"> File Manager</span></a></li>
                            <li><a href="icon.html"><i class="icon-star"></i><span class="hidden-tablet"> Icons</span></a></li>
                            <li><a href="login.html"><i class="icon-lock"></i><span class="hidden-tablet"> Login Page</span></a></li>-->
                        </ul>
                    </div>
                </div>
                <!-- end: Main Menu -->

                <noscript>
                <div class="alert alert-block span10">
                    <h4 class="alert-heading">Warning!</h4>
                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
                </div>
                </noscript>

                <!-- start: Content -->
                <div id="content" class="span10" style="">


                    @yield('admin_content')



                </div><!--/.fluid-container-->

                <!-- end: Content -->
            </div><!--/#content.span10-->
        </div><!--/fluid-row-->

        <div class="modal hide fade" id="myModal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Settings</h3>
            </div>
            <div class="modal-body">
                <p>Here settings can be configured...</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Close</a>
                <a href="#" class="btn btn-primary">Save changes</a>
            </div>
        </div>

        <div class="clearfix"></div>

        <footer>

            <p>
                <span style="display:table; text-align:center;">&copy; 2017 <a href="#" alt="Bootstrap_Metro_Dashboard">SJW  Dashboard</a></span>

            </p>

        </footer>

        <!-- start: JavaScript-->



        <script src="{{asset('admin_asset/js/jquery.cookie.js')}}"></script>

        <script src='{{asset('admin_asset/js/fullcalendar.min.js')}}'></script>

        <script src='{{asset('admin_asset/js/jquery.dataTables.min.js')}}'></script>

        <script src="{{asset('admin_asset/js/excanvas.js')}}"></script>
        <script src="{{asset('admin_asset/js/jquery.flot.js')}}"></script>
        <script src="{{asset('admin_asset/js/jquery.flot.pie.js')}}"></script>
        <script src="{{asset('admin_asset/js/jquery.flot.stack.js')}}"></script>
        <script src="{{asset('admin_asset/js/jquery.flot.resize.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.chosen.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.uniform.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.cleditor.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.noty.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.elfinder.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.raty.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.iphone.toggle.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.uploadify-3.1.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.gritter.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.imagesloaded.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.masonry.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.knob.modified.js')}}"></script>

        <script src="{{asset('admin_asset/js/jquery.sparkline.min.js')}}"></script>

        <script src="{{asset('admin_asset/js/counter.js')}}"></script>

        <script src="{{asset('admin_asset/js/retina.js')}}"></script>

        <script src="{{asset('admin_asset/js/custom.js')}}"></script>
        
        <script src="{{asset('public/templateEditor/ckeditor/ckeditor.js')}}"></script>

  <script>
            CKEDITOR.replace( 'editor1' );
            CKEDITOR.replace( 'editor2' );
            CKEDITOR.replace( 'editor3' );
        </script>
        <!-- end: JavaScript-->

    </body>
</html>
