@extends('admin.admin_master')
@section('admin_content')
<div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Category Name</th>  
                        {{-- <th>ক্যাটাগরি নাম </th>  --}}                  
                         <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    $i=1;
                    foreach ($categories as $category_info){
                    ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td class="center"><?php echo $category_info->category_name;?></td>
                                     
                        <td class="center">
                            <?php 
                            if($category_info->publication_status==1){
                            ?>
                            <span class="label label-success">Published</span>
                            <?php }else{ ?>
                             <span class="label label-important">Unpublished</span>
                            <?php }?>
                        </td>
                        <td class="center">
                            <?php 
                            if($category_info->publication_status==1){
                            ?>
                            <a class="btn btn-success" href="{{URL::to('/unpublished-category/'.$category_info->category_id)}}">
                                <i class="halflings-icon white thumbs_down"></i>  
                            </a>
                             <?php }else{ ?>
                            <a class="btn btn-danger" href="{{URL::to('/published-category/'.$category_info->category_id)}}">
                                <i class="halflings-icon thumbs_up"></i>  
                            </a>
                             <?php }?>
                            <a class="btn btn-info" href="{{URL::to('/category/'.$category_info->category_id.'/edit')}}">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-danger" href="{{URL::to('/delete-category/'.$category_info->category_id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    
                    <?php $i++; } ?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection