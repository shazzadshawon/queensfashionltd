@extends('admin.admin_master')
@section('admin_content')
<div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Product Name</th>
                        <th>Main Category </th>   
                        <th>Category </th>   
                        <th>Sub Category</th>   
                         <th>Qty</th> 
                         <th>Price.TK</th> 
                         <th>Date</th>
                         <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    $i=1;
                    foreach ($Product as $product_info){
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                            <td><?php echo $product_info->product_name; ?></td>
                            <td class="center"><?php echo $product_info->category_name; ?></td> 
                            <td class="center"><?php echo $product_info->sub_category_name; ?></td>
                             <td class="center"><?php echo $product_info->sub_sub_category_name; ?></td>
                            <td class="center"><?php echo $product_info->product_quantity; ?></td>
                            <td class="center"><?php echo $product_info->product_price; ?></td>
                            <td class="center"><?php echo date('M j, Y h:ia', strtotime($product_info->created_at)) ?></td>
                            <td class="center">
                            <?php 
                            if($product_info->publication_status==1){
                            ?>
                            <span class="label label-success">Published</span>
                            <?php }else if($product_info->publication_status==0){ ?>
                             <span class="label label-important">Unpublished</span>
                            <?php }else if($product_info->publication_status==2){ ?>
                             <span class="label label-Warning">Stock Out</span>
                            <?php }?>
                        </td>
                        <td class="center">
                            <?php 
                            if($product_info->publication_status==1){
                            ?>
                            <a class="btn btn-success" href="{{URL::to('/unpublished-product/'.$product_info->id)}}">
                                <i class="halflings-icon white thumbs_down"></i>  
                            </a>
                             <?php }else if($product_info->publication_status==0){ ?>
                            <a class="btn btn-danger" href="{{URL::to('/published-product/'.$product_info->id)}}">
                                <i class="halflings-icon thumbs_up"></i>  
                            </a>
                             <?php }?>
                            <a class="btn btn-info" href="{{URL::to('/product/'.$product_info->id.'/edit')}}">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-success" href="{{URL::to('/product/'.$product_info->id)}}">
                                <i class="halflings-icon white zoom-in"></i> 
                            </a>
                            <a class="btn btn-danger" href="{{URL::to('/delete-product/'.$product_info->id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    
                    <?php $i++;}?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection