@extends('admin.admin_master')
@section('admin_content')


 @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif

            <div class="row-fluid">
                <div class="span2 statbox purple" ontablet="span6" ondesktop="span3">
                    <div class="boxchart"><canvas width="64" height="60" style="display: inline-block; width: 64px; height: 60px; vertical-align: top;"></canvas></div>
                    <div class="number">{{ $visitor->counter }}<i class="icon-arrow-up"></i></div>
                    <div class="title"></div>
                    <div class="footer">
                        <a href="#">Total Visitors</a>
                    </div>  
                </div>
              
                <div class="span2 statbox green" ontablet="span6" ondesktop="span3">
                    <div class="boxchart"><canvas width="64" height="60" style="display: inline-block; width: 64px; height: 60px; vertical-align: top;"></canvas></div>
                    <div class="number">{{ count($pending_orders) }}<i class="icon-arrow-up"></i></div>
                    <div class="title"></div>
                    <div class="footer">
                        <a href="#"> Pending Orders</a>
                    </div>  
                </div>

                <div class="span2 statbox blue" ontablet="span6" ondesktop="span3">
                    <div class="boxchart"><canvas width="64" height="60" style="display: inline-block; width: 64px; height: 60px; vertical-align: top;"></canvas></div>
                    <div class="number">{{ count($products) }}<i class="icon-arrow-up"></i></div>
                    <div class="title"></div>
                    <div class="footer">
                        <a href="#"> Available Products</a>
                    </div>  
                </div>
                <div class="span2 statbox red" ontablet="span6" ondesktop="span3">
                    <div class="boxchart"><canvas width="64" height="60" style="display: inline-block; width: 64px; height: 60px; vertical-align: top;"></canvas></div>
                    <div class="number">{{ count($contacts) }}<i class="icon-arrow-up"></i></div>
                    <div class="title"></div>
                    <div class="footer">
                        <a href="#">Inbox Mails</a>
                    </div>  
                </div>

                 <div class="span2 statbox orange" ontablet="span6" ondesktop="span3">
                    <div class="boxchart"><canvas width="64" height="60" style="display: inline-block; width: 64px; height: 60px; vertical-align: top;"></canvas></div>
                    <div class="number">854<i class="icon-arrow-up"></i></div>
                    <div class="title">visits</div>
                    <div class="footer">
                        <a href="#"> read full report</a>
                    </div>  
                </div>
                 <div class="span2 statbox black" ontablet="span6" ondesktop="span3">
                    <div class="boxchart"><canvas width="64" height="60" style="display: inline-block; width: 64px; height: 60px; vertical-align: top;"></canvas></div>
                    <div class="number">{{ $visitor->daily_count }}<i class="icon-arrow-up"></i></div>
                    <div class="title"></div>
                    <div class="footer">
                        <a href="#"> New Visitors Today</a>
                    </div>  
                </div>
            </div>




    <div class="row-fluid">
<div class="span9">
    <div class="row-fluid">
            <div class="box span8">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Order Chart</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                    

        <div class="box-content">
             {!! $product_chart->render() !!}
        </div>
    </div>


    <div class="box span4" style="height: 295px;">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Visitors</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
                       
            <div class="box-content">
                {!! $visitor_chart->render() !!}
            </div>
        </div>

        </div>
<!--        <div class="row-fluid">
            <div class="box span12" ontablet="span12" ondesktop="span12" >
                        <div class="box-header">
                            <h2><i class="halflings-icon user"></i><span class="break"></span>Latest posts from our Facebook page</h2>
                            <div class="box-icon">
                                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                            </div>
                        </div>
                        <div class="box-content" style="height: 200px; overflow-y: scroll;">
                            <ul class="dashboard-list">
                            @foreach ($response as $res)
                            @if (!empty($res['message'])) 
                               <li>
                                @php
                          
                                    $h = "6"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
                                    $hm = $h * 60;
                                    $ms = $hm * 60;
                                    $mydate = gmdate("d-M-Y", strtotime($res['updated_time']));
                                    $mytime = gmdate("d-M-Y h:i A", strtotime($res['updated_time'])+ $ms);


                                    $rawdate = strtotime($mydate)+ $ms;
                                    $rawtime = strtotime($mytime);

                                @endphp 
                                    <a target="_blank" href="http://facebook.com/{{ $res['from']['id'] }}">
                                        <img class="avatar" alt="Lucas" src="{{ asset('public/user.jpg') }}">
                                    </a>
                                    <p><a target="_blank" href="http://facebook.com/{{ $res['from']['id'] }}">{{ $res['from']['name'] }}</a><br>
                                     <span class="text-muted"><a target="_blank" style="color: gray" href="http://facebook.com/{{ $res['id'] }}" target="_blank">{{ $mytime }}</a></span></p>
                                    <p>{{ $res['message'] }}</p>
                                                                   
                                </li>
                                @endif
                            @endforeach
                                
                            </ul>
                        </div>
                    </div>
        </div>

-->





    </div>

    <div class="span3">
      <div class="fb-page" data-href="https://www.facebook.com/shobarjonnoweb/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/shobarjonnoweb/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/shobarjonnoweb/">Shobarjonnoweb</a></blockquote></div>
    </div>






        </div>







@endsection