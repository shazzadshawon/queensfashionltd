@extends('admin.admin_master')
@section('admin_content')
<style>
    .invoice {
        position: relative;
        background: #fff;
        border: 1px solid #f4f4f4;
        padding: 20px;
        margin: 10px 25px;
    }
    .page-header {
        margin: 10px 0 20px 0;
        font-size: 22px;
    }
    
    
</style>
<section class="content content_content" style="width: 70%; margin: auto;">
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                                         @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif
                    <i class="fa fa-globe"></i> Queens Fashion.
                    <small class="pull-right">Date: <?php echo date('M j, Y ', strtotime($orders->created_at)); ?></small>
                </h2>
            </div>
                    </div>
      
        <div class="row invoice-info">
           
            <div class="col-sm-4 invoice-col">
               Shipping Info:
               <?php 
               $shipping_info = DB::table('shipping_addresses')->where('order_number',$orders->order_number)->first();
               ?>
                <address>
                    <strong>
                        {{$shipping_info->name }}                                 </strong>
                    <br>
                    Address:  {{$shipping_info->address }} 
                                                      <br>
                    Phone: {{$shipping_info->phone }} 
                                       <br>
                   {{--  Location: {{$shipping_info->location }}   --}}                              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Invoice #{{$orders->order_number}}</b><br>
                <br>
              
            </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Product</th>                                                      
                            <th>Code</th>
                            <th>Size</th>
                             <th>Qty</th> 
                            <th>Unit Price</th>
                            <th>Sub Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i=0;
                        $j=1;
               $order_product = DB::table('orders')->where('order_number',$orders->order_number)->get();
               ?>
                        @foreach($order_product as $product_info)
                        <tr>
                            <td>{{$j}}</td>
                            <td>{{$product_info->product_name}}</td>
                            <td>{{$product_info->product_code}}</td>
                            <td>{{$product_info->size}}</td>
                            <td>{{$product_info->product_quantity}}</td>
                            <td>{{$price=$product_info->product_price}} TK</td>
                            <td>{{$subtotal =$product_info->product_quantity*$product_info->product_price}} TK</td>
                            <?php 
                            $i=$subtotal + $i;
                            $j++;
                            ?>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
            </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row">
            <!-- accepted payments column -->
            <div class="col-md-12">
               
                <div class="table-responsive">
                    <table class="table">
                        <tbody>


                            <tr>
                                <th>Delivery Charge :</th>
                                <td> 
                                   @if($shipping_info->location==0)
                                   <p>Delivery Charge Free </p>
                                   @elseif($shipping_info->location==65)
                                   <p>65 TK</p>
                                   
                                   @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div><!-- /.col -->
        </div>
        <div class="row">
            <!-- accepted payments column -->
            <div class="col-md-12">
               
                <div class="table-responsive">
                    <table class="table">
                        <tbody>


                            <tr>
                                <th>Total:</th>
                                <td>  

                                   <p>{{ $i+$shipping_info->location }} TK</p>
                                   
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div><!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
<!--                <a href="" class="btn btn-default"><i class="fa fa-print"></i> Print</a>-->
                @if($orders->publication_status==0 || $orders->publication_status==2)
                <a class="btn btn-success pull-right" href="{{URL::to('/delivered/'.$orders->order_number)}}" style="margin-left: 5px;"><i class="fa fa-credit-card"></i> Deliver</a>
                <a class="btn btn-success pull-right" href="{{URL::to('/refuse/'.$orders->order_number)}}"><i class="fa fa-credit-card"></i> Refuse Order</a>
                @elseif($orders->publication_status==2)
                <a class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Refuse Order</a>
                @elseif($orders->publication_status==1)
                   <a class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Delivered</a>
                 @endif
<!--                <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>-->
            </div>
        </div>
    </section>
</section>
@endsection