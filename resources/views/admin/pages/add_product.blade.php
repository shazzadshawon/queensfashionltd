@extends('admin.admin_master')
@section('admin_content')
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Product</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
              @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3 style="text-align: center; color: green;"> {{Session::get('message')}}</h3>
</div>
      
             @endif
             
        
        <div class="box-content">
            <div class="box-content">
             	{!! Form::open(['route' => 'product.store','files'=>true]) !!}
                <fieldset>
                     <div class="control-group">
                        <label class="control-label" for="date01">Main Category</label>
                        <div class="controls">
                            <select name="category_id"  id="category" class="span6 typeahead>
                                <option value="0"> Select Main Category </option>
                                @foreach($categories as $category_info)
                                <option value="{{$category_info->category_id}}">{{$category_info->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">Category</label>
                        <div class="controls">
                            <select name="sub_category_id" id="subcategory" class="span6 typeahead">
                                <option value="0">====Select Category====</option>
                                @foreach($sub_categories as $subcategories_info)
                                @php
                                    $category_name = DB::table('categories')->where('category_id',$subcategories_info->category_id)->first();
                                @endphp
                                <option value="{{$subcategories_info->sub_category_id}}">{{$subcategories_info->sub_category_name}}
                             @if(!empty($category_name))
                                 {{  '('.$category_name->category_name.')'}}
                            @endif
                                </option>
                                @endforeach
                                
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">Sub Category</label>
                        <div class="controls">
                            <select name="sub_sub_category_id"  class="span6 typeahead" id="subsubcategory">
                                <option value="0">==== Select Sub Category ====</option>
                                @php
                                    $subsubcategories = DB::table('sub_categories')->where('publication_status',1)->get();
                                @endphp
                                @foreach($subsubcategories as $subcategoriesinfo)
                                @php
                                    $SubSubcategory_name = DB::table('sub_sub_categories')->where('sub_category_id',$subcategoriesinfo->sub_category_id)->get();
                                @endphp
                                @foreach($SubSubcategory_name as $SubSubcategory_nameInfo)
                                <option value="{{$SubSubcategory_nameInfo->id}}">{{$SubSubcategory_nameInfo->sub_sub_category_name}}
                                    
                                 </option>
                                 @endforeach
                                @endforeach
                                
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product Name</label>
                        <div class="controls">
                            <input type="text"  name="product_name" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    {{-- <div class="control-group">
                        <label class="control-label" for="typeahead">প্রোডাক্ট নাম</label>
                        <div class="controls">
                            <input type="text"  name="product_name_bn" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div> --}}
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product Code</label>
                        <div class="controls">
                            <input type="text"  name="product_code" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product Price</label>
                        <div class="controls">
                            <input type="text"  name="product_price" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product Quantity</label>
                        <div class="controls">
                            <input type="text"  name="product_quantity" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Discount</label>
                        <div class="controls">
                            <input type="text"  name="discount" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">Product Description</label>
                        <div class="controls">
                            <textarea  name="description" class="" id="editor1" rows="3"></textarea>
                        </div>
                    </div>
                   {{--  <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">প্রোডাক্ট ডেসক্রিপশন </label>
                        <div class="controls">
                            <textarea  name="description_bn" class="cleditor" id="textarea2" rows="3"></textarea>
                        </div>
                    </div> --}}
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product Images</label>
                        <div class="controls">
                            {!! Form::file('product_image[]', array('multiple'=>true)) !!}
                           
                        </div>
                    </div>
                  {{--   <div class="control-group">
                        <label class="control-label" for="date01">Select Offer</label>
                        <div class="controls">
                            <select name="offer_status">
                                
                                <option value="1">Hot Deal</option>
                                 <option value="2">New Arrival</option>
                                  <option value="3">Special Offer</option>
                                                  
                                
                            </select>
                        </div>
                    </div> --}}
                    
                    <div class="control-group">
                        <label class="control-label" for="date01">Publication Status</label>
                        <div class="controls">
                            <select name="publication_status" class="span6 typeahead">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                                <option value="2">Stock Out</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection