@extends('admin.admin_master')
@section('admin_content')
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Form Elements</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
              @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif
        
        <div class="box-content">
            <div class="box-content">
             	{!! Form::model($category, ['route' => ['category.update',$category->category_id], 'method' => 'PUT', 'name'=>'edit_category']) !!}
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Category Name</label>
                        <div class="controls">
                            <input type="text"  name="category_name" value="{{$category->category_name}}" class="span6 typeahead">
                           
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">ক্যাটাগরি নাম </label>
                        <div class="controls">
                            <input type="text"  name="category_name_bn" value="{{$category->category_name_bn}}" class="span6 typeahead">
                           
                        </div>
                    </div>

                    
                    <div class="control-group">
                        <label class="control-label" for="date01">Publication Status</label>
                        <div class="controls">
                            <select name="publication_status">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>

<script>
    document.forms['edit_category'].elements['publication_status'].value = '{{$category->publication_status}}';    
</script>
@endsection

