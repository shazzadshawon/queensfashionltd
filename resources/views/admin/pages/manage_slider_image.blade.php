@extends('admin.admin_master')
@section('admin_content')
<div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Image id</th>
                        <th>Image</th>                     
                         <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                    foreach ($slider_image as $slider_image_info){
                    ?>
                    <tr>
                        <td><?php echo $slider_image_info->slider_image_id;?></td>
                        <td class="center"><img src="<?php echo $slider_image_info->slider_image;?>" style="height: 50px; width: 100px;"></td>                     
                        <td class="center">
                            <?php 
                            if($slider_image_info->publication_status==1){
                            ?>
                            <span class="label label-success">Published</span>
                            <?php }else{ ?>
                             <span class="label label-important">Unpublished</span>
                            <?php }?>
                        </td>
                        <td class="center">
                            <?php 
                            if($slider_image_info->publication_status==1){
                            ?>
                            <a class="btn btn-success" href="{{URL::to('/unpublished-slider-image/'.$slider_image_info->slider_image_id)}}">
                                <i class="halflings-icon white thumbs_down"></i>  
                            </a>
                             <?php }else{ ?>
                            <a class="btn btn-danger" href="{{URL::to('/published-slider-image/'.$slider_image_info->slider_image_id)}}">
                                <i class="halflings-icon thumbs_up"></i>  
                            </a>
                             <?php }?>
                            <a class="btn btn-info" href="{{URL::to('/slider-image/'.$slider_image_info->slider_image_id.'/edit')}}">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-danger" href="{{URL::to('/delete-slider-image/'.$slider_image_info->slider_image_id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    
                    <?php }?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection