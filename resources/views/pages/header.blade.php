<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<div class="container main-header">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 logo">
                <a href="{{ URL::to('/') }}"><img alt="Kute shop - themelock.com" src="../images/logo.jpg" style="width: 280px;" /></a>
            </div>
             <div class="col-xs-7 col-sm-7 header-search-box">
                <div class="form-inline">
                     
                      <div class="form-group input-serach">
                        <input type="text"  placeholder="Keyword here..." id="search">
                      </div>
                      <button type="submit" class="pull-right btn-search"></button>
                </div>
                <div id="result"></div>
            </div>
            
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 group-button-header">
                    <div class="btn-cart" id="cart-block">
                        <a title="My cart" href="#">
                                            
                                               Shopping Cart
                                            
                        </a>
                        <span class="notify notify-right">
                            @php
                                 $i=0;
                        $j=0;
                        $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get(); 
                           @BNdphp                     
                        @foreach($addTocart as $cart_info)
                        @php
                        $i = $cart_info->product_quantity+$i;
                        @BNdphp
                        @BNdforeach

                        @php
                        
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $Qnt = str_replace($replace_array,$search_array,$i);
                        @BNdphp
                                         @if (Session::has('BN'))
                                                 {{ $i }}
                                            @else
                                                 {{ $Qnt }}
                                            @BNdif
                        </span>
                        <div class="cart-block">
                            <div class="cart-block-contBNt">
                                
                                <div class="cart-block-list">
                                    <ul>
                                @foreach($addTocart as $cart_info)
                                <li class="product-info">
                                    <div class="p-left">
                                        <a href="#" class="remove_link"></a>
                                        <a href="#">
                                        @php
                                        $ProductImage = DB::table('product_images')->where('product_id',$cart_info->product_id)->first(); 
                                    @BNdphp
                                        <img class="img-responsive" src="{{ URL::to($ProductImage->product_image) }}" alt="p10">
                                        </a>
                                    </div>
                                    <div class="p-right">
                                        <p class="p-name">
                                        @if (Session::has('BN'))
                                                {{ $cart_info->product_name }}
                                            @else
                                               {{ $cart_info->product_name_bn }}
                                            @BNdif
                                        
                                        </p>
                                        <p class="p-rice">
                                
                                            @php
                       
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $BnPrice = str_replace($replace_array,$search_array,$cart_info->product_price);
                                            @BNdphp
                                     
                                           @if (Session::has('BN'))
                                                 {{ $cart_info->product_price }} TK
                                            @else
                                                 {{ $BnPrice }} ট
                                            @BNdif
                                        </p>
                                        <p>
                                             @php
                       
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $QTY = str_replace($replace_array,$search_array,$cart_info->product_quantity);
                                            @BNdphp
                                     
                                           @if (Session::has('BN'))
                                                 Qty:{{ $cart_info->product_quantity }}
                                            @else
                                                পরিমাণ: {{ $QTY }} 
                                            @BNdif
                                       
                                          </p>
                                        @php
                                            $subTotal = $cart_info->product_price * $cart_info->product_quantity;
                                            $j = $j+$subTotal;
                                        @BNdphp
                                    </div>
                                </li>
                                @BNdforeach
                            </ul>
                                </div>
                                <div class="toal-cart">
                                    <span>
                                            @if (Session::has('BN'))
                                                 Total
                                            @else
                                                মোট
                                            @BNdif
                                    </span>
                                    <span class="toal-price pull-right">
                                         @php
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $BnT = str_replace($replace_array,$search_array,$j);
                                    @BNdphp
                                            @if (Session::has('BN'))
                                                 {{ $j }} TK
                                            @else
                                                 {{ $BnT }} ট
                                            @BNdif
                                    </span>
                                </div>
                                <div class="cart-buttons">
                                 @if (Session::has('customer_id'))
                                <a href="{{ URL::to('/shipping') }}" class="btn-check-out">
                                            @if (Session::has('BN'))
                                                 Checkout
                                            @else
                                                চেকআউট
                                            @BNdif
                                </a>
                                 @else
                                <a href="{{ URL::to('/User-Register') }}" class="btn-check-out">
                                    @if (Session::has('BN'))
                                                 Checkout
                                            @else
                                                চেকআউট
                                            @BNdif
                                </a>
                                 @BNdif
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

     <script type="text/javascript">
    $('#search').on('keyup',function(){
        $value=$(this).val();
        $.ajax({
            type : 'get',
            url  : '{{URL::to('search')}}',
            data : {'search':$value},
            success:function(data){
                $('#result').html(data);
            }
        });
    })
</script>

<style type="text/css">

    .web {
        margin: .1em 0 0.5em 0.7em;
    }
    .web input[type="submit"] {
        width: 16%;
    }
    .web{
        font-family:tahoma;
        size:12px;
        top:10%;
        /* border:1px solid #CDCDCD;*/
        border-radius:10px;
        padding:10px;
        width:100%;
        margin:auto;
        float: left;
    }
    #search_keyword_id
    {
        width: 70%;
        height: 51px;
        border:solid 1px #CDCDCD;
        padding:10px;
        font-size:14px;
        /* margin-left: 80px;*/
    }
    #result
    {
        position:absolute;
        width:auto;
    
        margin-left: 15%;
        margin-top:-1px;
        border-top:0px;
        overflow:hiddBN;
        border:1px #CDCDCD solid;
        background-color: white;
        z-index: 11;
    }
    .show
    {
        font-family:tahoma;
        padding:10px; 
        border-bottom:1px #CDCDCD dashed;
        font-size:15px; 
    }
    .show:hover
    {
        background:#bdc3c7;
        color:#fff;
        text-decoration: none;
        cursor:pointer;
    }
    .show a{
        color: #2c3e50;
        text-decoration: none;
        position: relative;
        z-index: 11;
    }
    .show a:hover{
        color: #d35400;
        text-decoration: none;

    }
</style>