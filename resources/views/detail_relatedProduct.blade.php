 @php
    $products = DB::table('products')
                 ->where('products.sub_category_id',$productInfo->sub_category_id)
                 ->join('categories', 'products.category_id', '=', 'categories.category_id')      
                 ->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.sub_category_id')
                 ->join('sub_sub_categories', 'products.sub_sub_category_id', '=', 'sub_sub_categories.id')
                ->select('products.*', 'categories.category_name','sub_categories.sub_category_name','sub_sub_categories.sub_sub_category_name')
                ->orderby('id','desc')
    ->get();
@endphp


 @foreach ($products as $pro)
                            @php
                               // $p = array($pro);
                            $image = DB::table('product_images')->where('product_id',$pro->id)->first();
                            @endphp
<li>
    <div class="product-container">
        <div class="left-block">
             <a href="{{ asset('product-details/'.$pro->id) }}">
                                                <img style="height: 250px" class="img-responsive" alt="product" src="{{ asset('product_image/'.$image->product_image) }}" />
                                            </a>
            <div class="quick-view">
                <a title="Add to my wishlist" class="heart" href="#"></a>
                
            </div>
            <div class="add-to-cart">
            	 {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!} 

            	 <input type="hidden" name="product_id" value="{{$pro->id}}">
                                <input type="hidden" name="product_name" value="{{$pro->product_name}}">
                                <input type="hidden" name="product_name_bn" value="{{$pro->product_name_bn}}">
                                <input type="hidden" name="product_code" value="{{$pro->product_code}}">
                                <input type="hidden" name="product_price" value="@if($pro->discount > 0){{ $pro->product_price-($pro->product_price*$pro->discount)/100}}@else{{$pro->product_price}}@endif">
                                <input type="hidden" name="product_quantity" value="1">
                                <input type="hidden" name="publication_status" value="{{$pro->publication_status}}">

                 <button class="btn-add-cart" type="submit">
                                       
                                                 Add To Cart
                                            
                                   </button>
                  {!! Form::close() !!}
            </div>
        </div>
         <div class="right-block">
                                            <h5 class="product-name"><a href="#">{{ $pro->product_name }}</a></h5>
                                           
                                            <div class="content_price">
                                                <span class="price product-price">&#2547; {{ $pro->product_price-($pro->product_price*$pro->discount)/100 }}</span>
                                                <span class="price old-price">&#2547;{{ $pro->product_price }}</span>
                                            </div>
                                        </div>
    </div>
</li>

@endforeach