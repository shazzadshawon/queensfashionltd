@extends('layouts.frontend')

@section('content')


<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
      
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">CATEGORIES</p>
                    <div class="block_content">
                        <!-- layered -->
                       
                        @include('sidebar_category')
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->

                
                <!-- sidebar_promotion -->
                @include('sidebar_promotion')
                <!--./sidebar_promotion-->

            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                  @php
                      $product_images = DB::table('product_images')->where('product_id',$productInfo->id)->get();
                  @endphp
                <!-- Product -->
                    <div id="product">
                        <div class="primary-box row">
                            <div class="pb-left-column col-xs-12 col-sm-6">
                                <!-- product-imge-->
                                <div class="product-image">
                                    <div class="product-full">
                                        <img id="product-zoom" src='{{ asset('product_image/'.$product_images[0]->product_image) }}' data-zoom-image="{{ asset('product_image/'.$product_images[0]->product_image) }}"/>
                                    </div>
                                    <div class="product-img-thumb" id="gallery_01">
                                        <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="true">
                                            @for($i=0; $i < count($product_images); $i++)
                                            <li>
                                                <a href="#" data-image="{{ asset('product_image/'.$product_images[$i]->product_image) }}" data-zoom-image="{{ asset('product_image/'.$product_images[$i]->product_image) }}">
                                                    <img id="product-zoom"  src="{{ asset('product_image/'.$product_images[$i]->product_image) }}" style="height: 95px" /> 
                                                </a>
                                            </li>
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                                <!-- product-imge-->
                            </div>
                            <div class="pb-right-column col-xs-12 col-sm-6">
                                <h1 class="product-name">{{ $productInfo->product_name }}</h1>
                                <div class="product-price-group">
                                   <span class="price">&#2547;{{ $productInfo->product_price-($productInfo->product_price*$productInfo->discount)/100 }}</span>
                                    <span class="old-price">&#2547;{{ $productInfo->product_price }}</span>
                                    <span class="discount">-{{ $productInfo->discount }}%</span>
                                </div>
                                <div class="info-orther">
                                    <p>Item Code: {{ $productInfo->product_code }}</p>
                                    <p>Availability: <span class="in-stock">
                                         @if ($productInfo->product_quantity >0)
                                           
                                                 Stock In
                                         
                                        @else
                                          
                                                 Stock Out
                                           
                                        @endif
                                    </span></p>
                                    <p>Condition: New</p>
                                </div>
                                    {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!} 
                                <div class="form-option">
                                    <p class="form-option-title">Available Options:</p>
                                   
                                    <div class="attributes">
                                        <div class="attribute-label">Qty:</div>
                                        <div class="attribute-list product-qty">
                                            <div class="qty">
                                                <input id="option-product-qty" name="product_quantity" type="text" value="1">
                                            </div>
                                            <div class="btn-plus">
                                                <a href="#" class="btn-plus-up">
                                                    <i class="fa fa-caret-up"></i>
                                                </a>
                                                <a href="#" class="btn-plus-down">
                                                    <i class="fa fa-caret-down"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="attributes">
                                        <div class="attribute-label">Size:</div>
                                        <div class="attribute-list">

                                            @php
                                                $sizes = DB::table('product_sizes')->where('product_id',$productInfo->id)->get();
                                            @endphp

                                            <select name="size">
                                                @foreach ($sizes as $size)
                                                    <option value="{{ $size->size }}">{{ $size->size }}</option>
                                                @endforeach
                                            </select>
                                           
                                        </div>
                                        
                                    </div>
                                </div>

                                <input type="hidden" name="product_id" value="{{$productInfo->id}}">
                                <input type="hidden" name="product_name" value="{{$productInfo->product_name}}">
                                <input type="hidden" name="product_name_bn" value="{{$productInfo->product_name_bn}}">
                                <input type="hidden" name="product_code" value="{{$productInfo->product_code}}">
                                <input type="hidden" name="product_price" value="@if($productInfo->discount > 0){{ $productInfo->product_price-($productInfo->product_price*$productInfo->discount)/100}}@else{{$productInfo->product_price}}@endif">
                                
                                <input type="hidden" name="publication_status" value="{{$productInfo->publication_status}}">

                                <div class="form-action">
                                    <div class="button-group">
                                        <button class="btn-add-cart" type="submit">
                                       
                                                 Add To Cart
                                            
                                        </button>
                                    </div>


                                     {!! Form::close() !!}



                                    <div class="button-group">
                                        @php
                                 
                                        // $wishlist = DB::table('wishlists')
                                        //         ->where('product_id', $productInfo->id)
                                        //         ->where('customer_id', Session::get('customer_id'))->first();
                                        @endphp
                                          
                                   {{-- wish --}}
                     
                                
                                        {!! Form::open(['route' => 'wishlist.store','files'=>true]) !!}
                                         <input type="hidden" name="customer_id" value="{{Session::get('customer_id')}}">
                                        <input type="hidden" name="product_id" value="{{$productInfo->id}}">
                                       <div class="">
                                      
                                        <button type="submit" title="Add to my wishlist" class="wishlist" value="W" style="color: green;"><i class="fa fa-heart-o"></i> Wishlist</button>
                                        
                                     
                                       </div>
                                         {!! Form::close() !!}
                                         
                                      
                                   
                                    </div>
                                </div>
                                <div class="form-share">
                                    <div class="sendtofriend-print">
                                        <a href="javascript:print();"><i class="fa fa-print"></i> Print</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab product -->
                        <div class="product-tab">
                            <ul class="nav-tab">
                                <li class="active">
                                    <a aria-expanded="false" data-toggle="tab" href="#product-detail">Product Details</a>
                                </li>
                                {{-- <li>
                                    <a aria-expanded="true" data-toggle="tab" href="#information">information</a>
                                </li> --}}
                            </ul>
                            <div class="tab-container">
                                <div id="product-detail" class="tab-panel active">
                                    <p>
                                        @php
                                            print_r( $productInfo->description );
                                        @endphp 
                                    </p>
                                </div>
                                {{-- <div id="information" class="tab-panel">
                                  
                                </div> --}}
                            </div>
                        </div>
                        <!-- ./tab product -->
                        <!-- box product -->
                        <div class="page-product-box">
                            <h3 class="heading">Related Products</h3>
                            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
                                
                                @include('detail_relatedProduct')
                            </ul>
                        </div>
                        <!-- ./box product -->
                        <!-- box product -->
                        
                        <!-- ./box product -->
                    </div>
                <!-- Product -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>



@endsection