@extends('layouts.frontend')

@section('content')



<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
      {{--   <div class="breadcrumb clearfix">
    <a class="home" href="{{url('/')}}" title="Return to Home">Home</a>
    <span class="navigation-pipe">&nbsp;</span>
    <span class="navigation_page">Category Name</span>
</div> --}}
        <!-- ./breadcrumb -->

        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- sidebar category -->
                <div class="block left-module">
                    <p class="title_block">All Categories</p>
                    <div class="block_content">
                        <!-- layered -->
                        
                        @include('sidebar_category')
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./sidebar category  -->
                
                <!-- sidebar_promotion -->
               
                @include('sidebar_promotion')
                <!--./sidebar_promotion-->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <div class="subcategories">
                    <ul>
                        <li>
                            <a href="{{ url('/') }}">Home</a>
                        </li>

                        <li class="current-categorie">
                            <a href="#">{{ $title }}</a>
                        </li>
                    </ul>
                </div>
                <!-- ./subcategories -->
                <!-- view-product-list-->
                <div id="view-product-list" class="view-product-list">
                    <h2 class="page-heading">
                        <span class="page-heading-title"> {{ $title }}</span>
                    </h2>
                    <ul class="display-product-option">
                        <li class="view-as-grid selected">
                            <span>grid</span>
                        </li>
                        <li class="view-as-list">
                            <span>list</span>
                        </li>
                    </ul>
                    <!-- PRODUCT LIST -->
                   
                    @include('category_product_list')
                    <!-- ./PRODUCT LIST -->
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>



@endsection