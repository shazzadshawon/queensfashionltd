@extends('admin.admin_master')
@section('admin_content') 
    <div class="">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">Complain Box</h3>
                                   
                                     <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                       
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>                               
                                </div>
                                <div class="panel-body">
                                   <div class="block">
                                        <form class="form-horizontal" method="POST" action="{{ url('postcomplain') }}" >      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Your Name</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="sjw_name"/>
                                        </div>
                                    </div>
                                   
 <div class="form-group">
                                        <label class="col-md-2 control-label">Your Email</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="sjw_email"/>
                                        </div>
                                    </div>
                                   
                                     
              

         
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Complain Description </label>
                                        <div class="col-md-10">
                                            <div class="">
                                                <textarea rows="5"  name="editor" class="form-control"></textarea>
       
                                            </div>
                                        </div>
                                    </div>

                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="brn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
                                   </div>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection