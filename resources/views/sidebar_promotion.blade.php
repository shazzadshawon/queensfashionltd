
@php
	$offers = DB::table('pazzles')->get();
@endphp
<div class="col-left-slide left-module">
    <ul class="owl-carousel owl-style2" data-loop="true" data-nav = "false" data-margin = "0" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1" data-autoplay="true">
    	@foreach ($offers as $offer)
    		<li><a href="#"><img style="height:400px;" src="{{ asset($offer->pazzle_image) }}" alt="slide-left"></a></li>
    	@endforeach
        

    </ul>
</div>