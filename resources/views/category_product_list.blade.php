<ul class="row product-list grid">
	 @foreach ($products as $productsInfo)
	    @php
	        $productsImage = DB::table('product_images')->where('product_id',$productsInfo->id)->first();
	    @endphp
    <li class="col-sx-12 col-sm-4">
        <div class="product-container">
            <div class="left-block">
                <a href="{{ URL::to('/product-details/'.$productsInfo->id) }}"><img class="img-responsive" alt="product" src="{{ asset('product_image/'.$productsImage->product_image) }}" style="height: 250px;" /></a>
                  {!! Form::open(['route' => 'wishlist.store','files'=>true]) !!}
                                         <input type="hidden" name="customer_id" value="{{Session::get('customer_id')}}">
                                        <input type="hidden" name="product_id" value="{{$productsInfo->id}}">
                <div class="quick-view">
                     <button type="submit" title="Add to my wishlist" class="heart" value="" style="color: green;"></button>
                    
                </div>
                {!! Form::close() !!}
                <div class="add-to-cart">
            	 {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!} 

            	 <input type="hidden" name="product_id" value="{{$productsInfo->id}}">
                                <input type="hidden" name="product_name" value="{{$productsInfo->product_name}}">
                                <input type="hidden" name="product_name_bn" value="{{$productsInfo->product_name_bn}}">
                                <input type="hidden" name="product_code" value="{{$productsInfo->product_code}}">
                                <input type="hidden" name="product_price" value="@if($productsInfo->discount > 0){{ $productsInfo->product_price-($productsInfo->product_price*$productsInfo->discount)/100}}@else{{$productsInfo->product_price}}@endif">
                                <input type="hidden" name="product_quantity" value="1">
                                <input type="hidden" name="publication_status" value="{{$productsInfo->publication_status}}">

                 <button class="" type="submit">
                                       
                                                 Add To Cart
                                            
                                   </button>
                  {!! Form::close() !!}
            </div>
            </div>
            <div class="right-block">
                <h5 class="product-name"><a href="{{ URL::to('/product-details/'.$productsInfo->id) }}">{{ $productsInfo->product_name }}</a></h5>
                @if ($productsInfo->discount > 0)
                     <div class="content_price">
                <span class="price product-price">{{ $productsInfo->product_price-($productsInfo->product_price*$productsInfo->discount)/100 }} TK</span>
                <span class="price old-price">{{ $productsInfo->product_price }} TK</span>
                     </div> 

                @else
                      <div class="content_price">
                <span class="price product-price">{{ $productsInfo->product_price }} TK </span>
               
                     </div>  
                @endif
                                        
                <div class="info-orther">
                    <p>Item Code: {{$productsInfo->product_code}}</p>
                    <p class="availability">Availability: <span>
                     @if ($productsInfo->product_quantity >0)
                                           
                                                 Stock In
                                         
                                        @else
                                          
                                                 Stock Out
                                           
                                        @endif</span></p>
                    <div class="product-desc">
                       <p>@php
                       	print_r($productsInfo->description);
                       @endphp</p>
                    </div>
                </div>
            </div>
        </div>
    </li>
    @endforeach


</ul>