@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->
{{-- @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h4 style="text-align: center;"> {{Session::get('message')}}</h4>
</div>
      
@endif --}}

<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{url('/')}}" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">About Us</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Infomations</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li class="active"><span></span><a href="{{ url('about-us') }}">About Us</a></li>
                                    <li><span></span><a href="{{ url('return-policy') }}">Return Policy</a></li>
                                    <li><span></span><a href="{{ url('refund') }}">Refund</a></li>
                                    <li><span></span><a href="{{ url('contact-us') }}">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
            </div>
            <!-- ./left colunm -->
                      <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">About Us</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">

                    <div class="panel panel-danger">
                        <div class="panel-header">
                            <img class="img-responsive" src="{{ asset('assets/images/aboutUsBanner.jpg') }}" style="width: 100%" alt="">
                        </div>
                        <div class="panel-body">
                            <p>Queen's Fashion World  a massive selection of great products from hundreds of leading national and international brands.  Queen's Fashion World offers women's men's and kids' apparel, footwear, handbags, fashion accessories, beauty products and much more, all under the same roof.</p>
                        </div>
                        <div class="panel-footer"> <strong>Queen's Fashion</strong> </div>
                    </div>
                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
<!-- Footer -->
@endsection