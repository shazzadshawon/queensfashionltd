@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->
{{-- @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h4 style="text-align: center;"> {{Session::get('message')}}</h4>
</div>
      
@endif

 --}}

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
       
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
           
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- category-slider -->
              
                <!-- ./category-slider -->
                <!-- subcategories -->
             
                <!-- ./subcategories -->
                <!-- view-product-list-->
                <div id="view-product-list" class="view-product-list">
                    <h2 class="page-heading">
                        <span class="page-heading-title"> 
                                Search Results
                              </span>
                    </h2>
                    {{-- <ul class="display-product-option">
                        <li class="view-as-grid selected">
                            <span>grid</span>
                        </li>
                        <li class="view-as-list">
                            <span>list</span>
                        </li>
                    </ul> --}}
                    <!-- PRODUCT LIST -->
                    <ul class="row product-list grid">

                    
                            @if(count($products) == 0)
                            <br><br>
                            <h4>No Item Found !</h4>
                          
                            @endif

                         
                            @foreach ($products as $productsInfo)
                                @php
                                    $productsImage = DB::table('product_images')->where('product_id',$productsInfo->id)->first();
                                @endphp
                            <li class="col-sx-12 col-sm-3">
                                <div class="left-block">
                                    <a href="{{ URL::to('/product-details/'.$productsInfo->id) }}"><img class="img-responsive" alt="product" src="{{ asset('product_image/'.$productsImage->product_image) }}" style="height: 250px;" /></a>
                                    <!-------------Wishlist-------------->
                               @php
                                 
                                        $wishlist = DB::table('wishlists')
                                                ->where('product_id', $productsInfo->id)
                                                ->where('customer_id', Session::get('customer_id'))->first();
                               @endphp
                                          
                                   
                     
                                        @if (Session::has('customer_id'))
                                      
                                         @if($wishlist==NULL)
                                        {!! Form::open(['route' => 'wishlist.store','files'=>true]) !!}
                                         <input type="hidden" name="customer_id" value="{{Session::get('customer_id')}}">
                                        <input type="hidden" name="product_id" value="{{$productsInfo->id}}">
                                       <div class="quick-view">
                                      
                                        <button type="submit" title="Add to my wishlist" class="fa fa-heart-o" value="W" style="color: green;"></button>
                                     
                                       </div>
                                         {!! Form::close() !!}
                                          @else
                                          <div class="quick-view">
                                        <a title="Add to my wishlist" class="fa fa-heart-o" style="color: blue;"></a>
                                     
                                             </div>
                                          @endif
                                        @else 
                                        <div class="quick-view">
                                        <a title="Add to my wishlist" class="fa fa-heart-o" href="{{URL::to('/User-Register')}}"></a>
                                     
                                        </div>
                                   
                                        @endif
                               
                            <!--------------wishlist--------- -->
                                   {{--  <div class="add-to-cart">
                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                    </div> --}}
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="#">
                                            
                                              {{ $productsInfo->product_name }}
                                           
                                        </a></h5>

                                        @php

    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $bn_number = str_replace($replace_array,$search_array,$productsInfo->product_price);

//     function en2bnNumber ($number){
                                          
//     $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
//     $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
//     $bn_number = str_replace($replace_array,$search_array,$number);

//     return $bn_number;
// }
                                          
                                        @endphp
                                       
                                      


                                             @if ($productsInfo->discount > 0)
                                                 <div class="content_price">
                                            <span class="price product-price">{{ $productsInfo->product_price-($productsInfo->product_price*$productsInfo->discount)/100 }} TK</span>
                                            <span class="price old-price">{{ $productsInfo->product_price }} TK</span>
                                                 </div> 

                                            @else
                                                  <div class="content_price">
                                            <span class="price product-price">{{ $productsInfo->product_price }} TK </span>
                                           
                                                 </div>  
                                            @endif
                                        


                                </div>
                            </li>
                            @endforeach
       
                        
                    </ul>
                    <!-- ./PRODUCT LIST -->
                </div>
                <!-- ./view-product-list-->
      {{--           <div class="sortPagiBar">
                    <div class="bottom-pagination">
                        <nav>
                          <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                              <a href="#" aria-label="Next">
                                <span aria-hidden="true">Next &raquo;</span>
                              </a>
                            </li>
                          </ul>
                        </nav>
                    </div>
                    <div class="show-product-item">
                        <select name="">
                            <option value="">Show 18</option>
                            <option value="">Show 20</option>
                            <option value="">Show 50</option>
                            <option value="">Show 100</option>
                        </select>
                    </div>
                    <div class="sort-product">
                        <select>
                            <option value="">Product Name</option>
                            <option value="">Price</option>
                        </select>
                        <div class="sort-product-icon">
                            <i class="fa fa-sort-alpha-asc"></i>
                        </div>
                    </div>
                </div> --}}
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

@endsection