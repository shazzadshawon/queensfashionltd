<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Category;
use App\SubCategory;
use App\SliderImage;
use App\Product;
use App\Customer;
use App\Wishlist;
use DB;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $categories = Category::where('publication_status', 1)
                ->take(5)
                ->get();
        return view('wish_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty(Session::get('customer_id')))
            {
                 Session::flash('message','Login Required !!!');
                return redirect()->back();
            }
         $wishlist = DB::table('wishlists')
                    ->where('product_id', $request->product_id)
                    ->where('customer_id', Session::get('customer_id'))->first();
            
        if(!empty($wishlist))
        {
             Session::flash('message','This Product is already in your wishlist !!!');
                return redirect()->back();
        }


        $wishlist = new Wishlist;
        $wishlist->product_id = $request->product_id;
        $wishlist->customer_id = $request->customer_id;   
        if($wishlist->save()){
            Session::flash('message','Product has been Added To Wishlist ');
        return redirect()->back();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Wishlist::where('id', $id)->delete();
        Session::flash('message', 'Your Selected Wishlist Product Has Been Remove Successfully ....!');
        return Redirect::to('/view-wishlist');
    }
}
