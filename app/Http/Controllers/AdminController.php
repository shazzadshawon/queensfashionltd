<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use Session;
//session_start();
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $id = Session::get('admin_id');
        if ($id != NULL) {
            return Redirect::to('/dashboard')->send();
        }
        else {
            return view('admin.login');
        }
    }

    public function AdminLoginCheck(Request $request) {
        $email_address = $request->email_address;
        $password = md5($request->password);

        $admins = DB::table('admins')
                ->where('email_address', $email_address)
                ->where('password', $password)
                ->first();
        if ($admins) {
            $request->Session()->put('admin_name', $admins->admin_name);
            $request->Session()->put('admin_id', $admins->admin_id);
            return Redirect::to('/dashboard');
        } else {
            Session::flash('message', 'Please Enter valid email & password!');
            return Redirect::to('/admin');
  
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
