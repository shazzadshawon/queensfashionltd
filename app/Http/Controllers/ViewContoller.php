<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Category;
use App\SubCategory;
use App\SliderImage;
use App\SubSubCategory;
use App\Product;
use App\Customer;
use App\Wishlist;
use DB;
class ViewContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $main_categories = DB::table('categories')->get();
        $mega_offer = DB::table('pazzles')->where('mega_offer',1)->get();
        //return $mega_offer;
        $sliders = DB::table('slider_images')->get();
        // $image = DB::table('product_images')->where('product_id',4)->get();
        // print_r($image);
        // exit();
                   
        return view('main',compact('sliders','main_categories','mega_offer'));
    }

    public function ProductPage($id) {
         $subCategory = SubCategory::where('sub_category_id',$id)->first();
         $products = Product::where('products.sub_category_id', $id)
          ->join('categories', 'products.category_id', '=', 'categories.category_id')      
                 ->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.sub_category_id')
                 ->join('sub_sub_categories', 'products.sub_sub_category_id', '=', 'sub_sub_categories.id')
                ->select('products.*', 'categories.category_name','sub_categories.sub_category_name','sub_sub_categories.sub_sub_category_name')
         ->where('products.publication_status',1)
                  ->orderBy('id', 'desc')
                 ->get();
        return view('category_products')->with('products',$products)->with('SubCategories',$subCategory);
    }
  

    public function cart() {
        return view('cart');
    }

    public function maincategorypage($id) {

         $subCategory = Category::where('category_id',$id)->first();
         $title = $subCategory->category_name;
         $products = Product::where('products.category_id', $id)
         ->join('categories', 'products.category_id', '=', 'categories.category_id')      
                 ->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.sub_category_id')
                 ->join('sub_sub_categories', 'products.sub_sub_category_id', '=', 'sub_sub_categories.id')
                ->select('products.*', 'categories.category_name','sub_categories.sub_category_name','sub_sub_categories.sub_sub_category_name')
         ->where('products.publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('category_products',compact('title'))->with('products',$products)->with('SubCategories',$subCategory);
    }

    public function categorypage($id) {
         $subCategory = SubCategory::where('sub_category_id',$id)->first();
         $title = $subCategory->sub_category_name;
         $products = Product::where('products.sub_category_id', $id)
         ->join('categories', 'products.category_id', '=', 'categories.category_id')      
                 ->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.sub_category_id')
                 ->join('sub_sub_categories', 'products.sub_sub_category_id', '=', 'sub_sub_categories.id')
                ->select('products.*', 'categories.category_name','sub_categories.sub_category_name','sub_sub_categories.sub_sub_category_name')
         ->where('products.publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('category_products',compact('title'))->with('products',$products)->with('SubCategories',$subCategory);
    }
  public function subcategorypage($id) {
         $subCategory = SubSubCategory::where('id',$id)->first();
         $title = $subCategory->sub_sub_category_name;
         $products = Product::where('products.sub_sub_category_id', $id)
         ->join('categories', 'products.category_id', '=', 'categories.category_id')      
                 ->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.sub_category_id')
                 ->join('sub_sub_categories', 'products.sub_sub_category_id', '=', 'sub_sub_categories.id')
                ->select('products.*', 'categories.category_name','sub_categories.sub_category_name','sub_sub_categories.sub_sub_category_name')
         ->where('products.publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('category_products',compact('title'))->with('products',$products)->with('SubCategories',$subCategory);
    }

    public function CrazyDeal($id) {
     
         $products = Product::where('offer_status', $id)
         ->where('publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('crazy_deal')->with('products',$products)->with('id',$id);
    }


    public function OfferProduct($id) {
     
         $products = Product::where('offer_status', $id)
         ->where('publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('offer_product')->with('products',$products)->with('id',$id);
    }
    


    public function SingleProductPage($id) {
     
         $productInfo = DB::table('products')->where('id',$id)->first();
         $categories = DB::table('categories')->get();
                
        return view('single',compact('productInfo','categories'));
    }
    
    public function CustomerLogin() {
        
        return view('login');
    }
     public function CustomerSignUp() {
        
        return view('registration');
    }
    public function about_us() {
        
        return view('about_us');
    }
    public function contact_us() {
        
        return view('contact_us');
    }

     public function md_message() {
        
        return view('md_message');
    }

    public function treamCondition() {
        
        return view('delivery_policy');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
           'customer_name'=>'required|max:255'
       ));
        $cuntomer = new Customer;
      
       $cuntomer->customer_name = $request->customer_name;
       $cuntomer->phone_number = $request->phone_number;
       $cuntomer->address = $request->address;
       $cuntomer->email_adderss = $request->email_adderss;
       $cuntomer->password = md5($request->password);
      
       if($cuntomer->save()){
              $request->Session()->put('customer_name',$cuntomer->customer_name);
              $request->Session()->put('customer_id',$cuntomer->id);
           Session::flash('message','Signup has been Created Successfully ....!');
        return Redirect::to('/shipping');
       }else{
        Session::flash('message','not valide info ....!');
        return Redirect::to('/Login-Customer');
       }
    }
public function CustomerLoginCheck(Request $request) {      

        //echo "login";
        //return view('admin.admin_master');
        $email_address = $request->email_adderss;

        $password = md5($request->password);
                
      
 
        $result = DB::table('customers')
                ->where('email_adderss', $email_address)    
                ->where('password', $password)
                ->first();
        
      
        
        if ($result) {
            //return view('admin.admin_master');
           $request->Session()->put('customer_name',$result->customer_name);
              $request->Session()->put('customer_id',$result->id);
              Session::flash('message','You are successfully  Login!');
             return Redirect::to('/shipping');
        } else {
            Session::flash('message','User Id or Password Invalid');
            return Redirect::to('/User-Register');
        }
   
        
    }
    
       public function logoutcustomer()
    {
           Session::put('customer_name',null);
            Session::put('customer_id',null);

               Session::flash('message','You are successfully  Logout!');
       return Redirect::to('/User-Register');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function post_contact(Request $request)
    {
        //return $request->all();

        DB::table('contacts')->insert(
            [
                'contact_title' => $request->get('contact_title'),
                'contact_email' => $request->get('contact_email'),
                'contact_reference' => $request->get('contact_reference'),
                'contact_description' => $request->get('contact_description'),
                'contact_title' => $request->get('contact_title'),
            ]);
            
             $to = "help@queenfashionworld.com";
$subject = $request->get('contact_title');
$txt =  $request->get('contact_description');
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <'.$request->get('contact_email').'>' . "\r\n";
//$headers .= 'Cc: akramul@agvcorp.com' . "\r\n";


mail($to,$subject,$txt,$headers);

Session::flash('message','Mesage sent successfully');
        return redirect()->back();


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function product_search(Request $request)
    {
       // return Input::all();
        //return $request->all();
        $cat_id = $request->get('cat');
        $query = $request->get('query');
        if ($cat_id==0) {
            $products = DB::table('products')->where('product_name', 'LIKE', '%'.$query.'%')->get();
        }
        else{
            $products = DB::table('products')
            ->where('category_id',$cat_id)
            ->where('product_name', 'LIKE', '%'.$query.'%')->get();
        }
        return view('product_search',compact('products'));
    }

}
