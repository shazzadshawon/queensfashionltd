<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use Redirect;
use Session;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\Product;
use App\ProductImage;
use App\product_size;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $product = DB::table('products')
                 ->join('categories', 'products.category_id', '=', 'categories.category_id')      
                 ->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.sub_category_id')
                 ->join('sub_sub_categories', 'products.sub_sub_category_id', '=', 'sub_sub_categories.id')
                ->select('products.*', 'categories.category_name','sub_categories.sub_category_name','sub_sub_categories.sub_sub_category_name')
                ->orderby('id','desc')
                ->get();
        return view('admin.pages.manage_product')->with('Product',$product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('publication_status',1)->get();
        $sub_categories = SubCategory::where('publication_status',1)->get();

        return view('admin.pages.add_product')->withCategories($categories)->withSubCategories($sub_categories);
    }
    
     public function SubCategory($id)
    {
        $sub_categories = SubCategory::where('category_id',$id)->get();

        return view('admin.pages.select_sub_categories')->withSubCategories($sub_categories);
    }
    
 public function AddProductImage(Request $request, $id) {
        $files = $request->file('product_image');
      // Making counting of uploaded images
      $file_count = count($files);
      // start count how many uploaded
      $uploadcount = 0;

      foreach ($files as $file) {
        $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(array('file'=> $file), $rules);
        if($validator->passes()){
          $destinationPath = 'product_image/'; // upload folder in public directory
          $filename = $file->getClientOriginalName();
          $upload_success = $file->move($destinationPath, $filename);
          $uploadcount ++;

          // save into database
          $extension = $file->getClientOriginalExtension();
          $entry = new ProductImage();          
          $entry->product_image = $filename;
          $entry->product_id = $id;
          $entry->save();
        }
      }
      if($uploadcount == $file_count){
        Session::flash('message', 'Your Product Images Has Been Uploaded successfully...!');
        return Redirect::to('/product/'.$id);
      } else {
          Session::flash('message', 'Your Product Images Has Been Uploaded successfully...!');
      return Redirect::to('/product/'.$id);
      }
    }

    public function AddProductSize(Request $request, $id) {      

          $product_size = new product_size();
          $product_size->product_id = $id;
          $product_size->size = $request->size;       
     
      
      if($product_size->save()){
        Session::flash('message', 'Your Product Size Has Been Uploaded successfully...!');
        return Redirect::to('/product/'.$id);
      } else {
          Session::flash('message', 'Your Product Size Has Been not Uploaded ...!');
      return Redirect::to('/product/'.$id);
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,array(
           'category_id'=>'required|max:255',
           'product_image'=>'required'
       ));
        $Product = new Product();
        $Product->category_id = $request->category_id;
        $Product->sub_category_id = $request->sub_category_id;
        $Product->sub_sub_category_id = $request->sub_sub_category_id;
        $Product->product_name = $request->product_name;
        $Product->product_name_bn = 'null';
        $Product->product_code = $request->product_code;
        $Product->product_price = $request->product_price;
        $Product->product_quantity = $request->product_quantity;
        $Product->discount = $request->discount;
        $Product->description = $request->description;
        $Product->description_bn = 'null';
        $Product->offer_status = $request->offer_status;
        $Product->publication_status = $request->publication_status;  
 
           if($Product->save()){
        $files = $request->file('product_image');
      // Making counting of uploaded images
      $file_count = count($files);
      // start count how many uploaded
      $uploadcount = 0;

      foreach ($files as $file) {
        $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){
              $destinationPath = 'product_image/'; // upload folder in public directory
              $filename = $file->getClientOriginalName();
              $upload_success = $file->move($destinationPath, $filename);
              $uploadcount ++;

              // save
              $extension = $file->getClientOriginalExtension();
              $product_image = new ProductImage();      
              $product_image->product_id =  $Product->id;
              $product_image->product_image = $filename;         
              $product_image->save();
            }
        }
         if($uploadcount == $file_count){
        Session::flash('message', 'Your Product Information Has Been Uploaded successfully...!');
        return Redirect::to('/add-product');
      } else {
          Session::flash('message', 'Product Has Ben Not Uploaded! Please Input Valid Data ..');
        return Redirect::to('/add-product');
      }
           }  else {
                 Session::flash('message', '');
        return Redirect::to('/add-product');
           }
    }

          public function unpublished($id) {

         $product = Product::where('id',$id)
                ->update(['publication_status' => 0]);
        Session::flash('message', 'Your Selected Product Has Been Unpublished Successfully..!');
        return Redirect::to('/manage-product');
    }
    
      public function published( $id)
    {

        $product = Product::where('id',$id)
                ->update(['publication_status' =>1]);

            Session::flash('message', 'Your Selected Product Has Been published Successfully..!');
            return Redirect::to('/manage-product');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, $id)
    {
         //return 1;
         $product = DB::table('products')->where('id',$id)
                ->leftjoin('categories', 'products.category_id', '=', 'categories.category_id')      
                 ->leftjoin('sub_categories', 'products.sub_category_id', '=', 'sub_categories.sub_category_id')      
                ->select('products.*', 'categories.category_name','sub_categories.sub_category_name')
                ->first();
                // var_dump($product);
                // exit;
         $ProductImage=DB::table('product_images')->where('product_id',$id)->get();



        session(['service_id' => $product->id]);
        session(['service_title' => $product->product_name]);
        session(['service_description' => $product->description]);
         
        // if(!session_id()) {
        //     session_start();
        //     //$_SESSION["post_id"] = $cat->id;
        //     session(['wed' => '']);
        //     session(['wed' => $service->id]);
        // }
        
 
      
         $login_link = $fb
            ->getRedirectLoginHelper()
            ->getLoginUrl('http://lakespoint.net/product_callback', ['email']);
           // ->getLoginUrl('localhost/red_done_n/service_callback', ['email', 'user_events']);
        
         



        return view('admin.pages.view_product',compact('login_link'))->with('product_info',$product)->with('image',$ProductImage);
    }




    public function product_callback(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb) {
       //service_description


         if(!session_id()) {
             session_start();
         }
         

         $post_id =  session('service_id');
         $post_title =  session('service_title');
         $post_description =  session('service_description');
       
        // $FbAppInfo = new FbAppInfo();
        //         $fb = $FbAppInfo->GetFbAppInfo();
        
        $helper = $fb->getRedirectLoginHelper();
        
        try {
          $accessToken = $helper->getAccessToken();
          //return typeof($accessToken);
          //$accessToken = $helper->getAccessToken();
          //echo $accessToken; exit;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
         
            return redirect()->back()->with('danger',$e->getMessage());
            
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          return redirect()->back()->with('danger',$e->getMessage());
        }
        
        if (! isset($accessToken)) {
          if ($helper->getError()) {
            header('HTTP/1.0 401 Unauthorized');
//            echo "Error: " . $helper->getError() . "\n";
//            echo "Error Code: " . $helper->getErrorCode() . "\n";
//            echo "Error Reason: " . $helper->getErrorReason() . "\n";
         //echo "Error Description: " . $helper->getErrorDescription() . "\n";
              return redirect()->back()->with('danger',$helper->getErrorDescription());
          } else {
            header('HTTP/1.0 400 Bad Request');
            return redirect()->back()->with('danger','Bad Request');
          }
         // exit;
        }
        
        // Logged in
        
       // echo '<h3>Access Token</h3>';
       // var_dump($accessToken->getValue());
        
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        
        
       
        //echo '<h3>Metadata</h3>';
       // var_dump($tokenMetadata);
        
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('1923089044631336'); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();
        
        if (! $accessToken->isLongLived()) {
          // Exchanges a short-lived access token for a long-lived one
          try {
            //$accessToken = $oAuth2Client->getLongLivedAccessToken('EAABeioWfpIgBAHOc0XTZBwVKb4PZAbhzOfkiZAy3LoZCLPZAu9WlYkrjpEPC75RMhNZAnxLyyztpFRMZCpoyOZC7XgxXKNLMUEtw3ZCi3GqL06gVdQOmdtKL5wTHpDy3Xr7XIVt8Fq6ZBobmOMso49r8iHgJ6AsYVcc6gnu6ZBMO6Jea5eJLsU2rQvZCqs42UZBwmdZChhgSD3zeBZA9QZDZD');
          $accessToken = $oAuth2Client->getLongLivedAccessToken('1923089044631336|kmF7Hc5nRadVcCpC2K0ikF2U7LA');
                                                                    
          } catch (Facebook\Exceptions\FacebookSDKException $e) {
            
              return redirect()->back()->with('danger',$e->getMessage());
           
              
            //  echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
           // exit;
          }
        
          //echo '<h3>Long-lived</h3>';
          //$access = $accessToken->getValue()
            
         // var_dump($accessToken->getValue());
        }
        
        $_SESSION['fb_access_token'] = (string) $accessToken;
        
        // User is logged in with a long-lived access token.
        // You can redirect them to a members-only page.
        //header('Location: https://example.com/members.php');
        
        
        
          // define your POST parameters (replace with your own values)
                $params = array(
                  "access_token" => $accessToken, // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
                  "message" =>  $post_title,
                  "link" => "http://lakespoint.net/product_callback/".$post_id,
                  "description" => $post_description,
                
                  "name" => "Shobarjonnoweb",
                 
                 
                );
                 
                // post to Facebook
                // see: https://developers.facebook.com/docs/reference/php/facebook-api/
                try {
                  $ret = $fb->post('/334422027017070/feed', $params);
                  return redirect('/shobarjonnoweb')->with('success','Successfully posted to Facebook');
                } catch(Exception $e) {
                  //echo $e->getMessage();
                  return redirect()->back()->with('danger',$e->getMessage());
                }
        


    }



    public function edit($id) {
        $products = Product::find($id);
        $categories = Category::where('publication_status', 1)->get();
        $sub_categories = SubCategory::where('publication_status', 1)->get();
        //$sub_sub_categories = SubSubCategory::where('publication_status', 1)->get();
        //var_dump(count($sub_sub_categories)); exit;
        return view('admin.pages.edit_product')->withProducts($products)->withCategories($categories)->withSubCategories($sub_categories);
    }







//     public function EditProductImage($id) {
//      $product_image = ProductImage::where('product_image_id',$id)->first();
//       return view('admin.pages.edit_product_image')->withProductImage($product_image);
//    }



    public function update(Request $request, $id)
    {
        $product = Product::where('id', $id)
                ->update([
            'category_id' => $request->category_id,
            'sub_category_id' => $request->sub_category_id,
            'product_name' => $request->product_name,
            'product_name_bn' => $request->product_name_bn,
            'product_code' => $request->product_code,
            'product_price' => $request->product_price,
            'product_quantity' => $request->product_quantity,
            'discount' => $request->discount,
            'description' => $request->description,
            'description_bn' => $request->description_bn,
            'offer_status' => $request->offer_status,
            'publication_status' => $request->publication_status
        ]);
        Session::flash('message', ' Product Information Has Been Updated Successfully..!');
            return Redirect::to('/manage-product');
    }
    
//        public function UpdateProductImage(Request $request, $product_id,$product_image_id)
//    {
//
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id',$id)->delete();
        ProductImage::where('product_id',$id)->delete();
        Session::flash('message', 'Your Selected Product Has Been Deleted Successfully ....!');
            return Redirect::to('/manage-product');
    }
    
    public function DeleteProductImage($product_id,$product_image_id)
    {
       
        ProductImage::where('product_image_id',$product_image_id)->delete();
        Session::flash('message', 'Your Selected Product Image Has Been Deleted Successfully ....!');
            return Redirect::to('/product/'.$product_id);
    }

    public function DeleteProductSize($product_id,$id)
    {
       
        product_size::where('id',$id)->delete();
        Session::flash('message', 'Your Selected Product Image Has Been Deleted Successfully ....!');
            return Redirect::to('/product/'.$product_id);
    }
    
    public function myformAjax($id)
    {
        $cities = DB::table("sub_categories")
                    ->where("category_id",$id);
                   
        return json_encode($cities);
    }
}
