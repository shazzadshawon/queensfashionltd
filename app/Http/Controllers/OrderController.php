<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App\AddToCart;
use App\Category;
use App\Product;
use App\Order;
use App\ShippingAddress;
use DB;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $order = DB::table('orders')
               ->join('customers', 'orders.customer_id', '=', 'customers.id')
               ->select('orders.*', 'customers.customer_name')
              ->groupBy('order_number')
               ->get(); 
       return view('admin.pages.manage_order')->with('orders',$order);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //    $this->validate($request,array(
       //     'name'=>'required|max:200',
       //      'phone'=>'required|200',
       //        'address'=>'required|200',
       //          'location'=>'required'
       // ));
         $order_number =  rand();
         
         $shipping_address = new ShippingAddress;
         $shipping_address->order_number= $order_number;
         $shipping_address->name= $request->name;
         $shipping_address->phone= $request->phone;
         $shipping_address->address= $request->address;
         $shipping_address->location= $request->location;
        
         if( $shipping_address->save()){
        $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get(); 
        
        foreach ($addTocart as $cart){
            $order = new Order;
            $order->product_id = $cart->product_id;
            $order->customer_id = Session::get('customer_id');
            $order->product_name = $cart->product_name;
            $order->product_code = $cart->product_code;
            $order->product_price = $cart->product_price;
            $order->product_quantity = $cart->product_quantity;
            $order->size = $cart->size;
            $order->order_number = $order_number;
            $order->order_date = date('d-m-Y');
            $order->session_id = Session::getId();
            
            if($order->save()){
                AddToCart::where('id',$cart->id)->delete();
            }
        }
         }
          Session::flash('message', 'Your Order Has Been placed !');
            return Redirect::to('/shipping');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }
    
    public function ViewOrder($id)
    {
         $order = DB::table('orders')->where('order_number',$id)
               ->join('customers', 'orders.customer_id', '=', 'customers.id')
               ->select('orders.*', 'customers.*')
               ->groupBy('order_number')
               ->first(); 
        return view('admin.pages.view_order')->with('orders',$order);
    }
    
    public function delivered($id)
    {
        $order = DB::table('orders')
                ->where('order_number', $id)
                ->get();
        foreach ($order as $oder_info) {

            $product_info = DB::table('products')
                    ->where('id', $oder_info->product_id)
                    ->first();
            $order_qty = $oder_info->product_quantity;
            $product_qty = $product_info->product_quantity;
            $qty = $product_qty-$order_qty;
            
             $product = Product::where('id',$oder_info->product_id)
                ->update(['product_quantity' =>$qty]);
             $order_status = Order::where('order_number',$id)
                ->update(['publication_status' =>1]);
        }
        Session::flash('message', 'Order Has Been delivered Successfully..!');
        return Redirect::to('/view-order/' . $id);
    }
    
      public function refuse($id)
    {
        $order = DB::table('orders')
                ->where('order_number', $id)
                ->get();
        foreach ($order as $oder_info) {    
             $order_status = Order::where('order_number',$id)
                ->update(['publication_status' =>2]);
        }
        Session::flash('message', 'Your Selected Order Has Been Refuse Successfully..!');
        return Redirect::to('/view-order/' . $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::where('order_number',$id)->delete();
        Session::flash('message', 'Your Selected Order Has Been Deleted Successfully..!');
        return Redirect::to('/manage-order');
    }
}
